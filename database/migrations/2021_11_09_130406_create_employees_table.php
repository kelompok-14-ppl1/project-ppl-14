<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->string('id_karyawan', 30)->primary();
            $table->string('nama_karyawan', 30);
            $table->string('tmpt_lahir',50 );
            $table->date('tggl_lahir');
            $table->enum('jenis_kel',['P','L']);
            $table->text('alamat_kary');
            $table->integer('status_kary')->default(1);
            $table->string('id_user');
            $table->foreign('id_user')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karyawan');
    }
}
