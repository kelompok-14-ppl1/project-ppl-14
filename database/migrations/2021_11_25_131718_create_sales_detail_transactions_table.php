<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesDetailTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_detail_transactions', function (Blueprint $table) {
            $table->id('id_detail_trans_penjualan');
            $table->string('nama_barang');
            $table->integer('harga_barang');
            $table->integer('jumlah_barang');
            $table->integer('total_harga_penjualan');
            $table->string('id_barang');
            $table->string('id_transaksi_penjualan');
            $table->foreign('id_barang')->references('id_barang')->on('products');
            $table->foreign('id_transaksi_penjualan')->references('id_transaksi_penjualan')->on('sales_transactions');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_detail_transactions');
    }
}
