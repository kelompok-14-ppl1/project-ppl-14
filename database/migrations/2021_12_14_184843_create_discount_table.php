<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount', function (Blueprint $table) {
            $table->id();
//            $table->String('barang')->unsigned();
//            $table->integer('harga_awal')->default(0);
//            $table->integer('harga_akhir')->default(0);
//            $table->integer('diskon_persen');
//            $table->integer('diskon_persen');
//            $table->date('tanggal awal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount');
    }
}
