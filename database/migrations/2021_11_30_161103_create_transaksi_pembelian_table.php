<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiPembelianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_pembelian', function (Blueprint $table) {
            $table->string('nomor_transaksi')->primary();
            $table->dateTime('tanggal_pembelian');
            $table->string('id_karyawan')->nullable(false);
            $table->unsignedBigInteger('id_supplier');
            $table->integer('nomor');
            $table->foreign('id_supplier')->references('id')->on('suppliers');
            $table->foreign('id_karyawan')->references('id_karyawan')->on('employees');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_pembelian');
    }
}
