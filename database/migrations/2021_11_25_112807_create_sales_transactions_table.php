<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_transactions', function (Blueprint $table) {
            $table->string('id_transaksi_penjualan')->primary();
            $table->dateTime('tanggal_transaksi_penjualan');
            $table->string('id_member_trans')->nullable(true);
            $table->string('id_karyawan_penjualan');
            $table->string('nomor');
            $table->integer('diskon')->default(0);
            $table->foreign('id_member_trans')->references('id_member')->on('members');
            $table->foreign('id_karyawan_penjualan')->references('id_karyawan')->on('employees');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_transactions');
    }
}
