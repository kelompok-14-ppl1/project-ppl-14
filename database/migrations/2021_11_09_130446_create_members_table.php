<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->string('id_member')->primary();
            $table->string('nama_member', 30);
            $table->string('tempat_lahir',50 );
            $table->date('tanggal_lahir');
            $table->enum('jenis_kelamin', ['P','L']);
            $table->text('alamat_member');
            $table->string('contact_member',13);
            $table->integer('status_member')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member');
    }
}
