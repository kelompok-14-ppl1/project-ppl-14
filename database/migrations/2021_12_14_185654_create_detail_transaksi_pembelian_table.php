<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailTransaksiPembelianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_transaksi_pembelian', function (Blueprint $table) {
            $table->id('id_detail_transaksi');
            $table->string('itrx_nama_barang');
            $table->integer('itrx_harga_barang');
            $table->integer('itrx_jumlah_barang');
            $table->integer('itrx_total_pembelian');
            $table->string('itrx_id_transaksi');
            $table->string('itrx_id_barang');
            $table->foreign('itrx_id_transaksi')->references('nomor_transaksi')->on('transaksi_pembelian');
            $table->foreign('itrx_id_barang')->references('id_barang')->on('products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_transaksi_pembelian');
    }
}
