@extends('layout.main')
@section('content')

    <div class="card">
        <div class="card-header">
            <p class="text-dark bold">{{ $title }}</p>
            <hr>
            @if (session('success'))
                <div class="alert-success">
                    <p>{{ session('success') }}</p>
                </div>
                error
            @endif
        </div>
        <div class="card-body">
            <a class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#tambah-data">Tambah Karyawan</a>
            <a href="{{route('karyawan.cetakpdf')}}" class="btn btn-primary" target="_blank">Export Pdf</a>
            <a href="{{route('karyawan.cetakExcel')}}" class="btn btn-primary" target="_blank">Export Excel</a>
            <div class="table-responsive">
                <table class="table table-primary" id="dataTable">
                    <thead class="thead-dark">
                    <tr>
                        <th>No</th>
                        <th>ID User</th>
                        <th>ID Karyawan</th>
                        <th>Nama Karyawan</th>
                        <th>Tempat Lahir</th>
                        <th>Tanggal lahir</th>
                        <th>Jenis Kelamin</th>
                        <th>Alamat Karyawan</th>
                        <th>Status Karyawan</th>
                        <th>Ubah Status Karyawan</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i = 1)
                    @foreach($all_karyawan as $row)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$row->id_user}}</td>
                            <td>{{$row->id_karyawan}}</td>
                            <td>{{$row->nama_karyawan}}</td>
                            <td>{{$row->tmpt_lahir}}</td>
                            <td>{{$row->tggl_lahir}}</td>
                            <td>{{$row->jenis_kel == 'L' ? 'Laki-Laki' : 'Perempuan'}}</td>
                            <td>{{$row->alamat_kary}}</td>
                            <td>{{$row->status_kary == '1' ? 'Aktif' : 'Tidak Aktif'}}</td>
                            <td>@if($row->status_kary == '1')
                                    <a href="{{route('karyawan.status_update',[$row->id_karyawan])}}"
                                       class="btn btn-sm btn-success">Aktif</a>
                                @else
                                    <a href="{{route('karyawan.status_update',[$row->id_karyawan])}}"
                                       class="btn btn-sm btn-danger">Non-Aktif</a>

                                @endif
                            </td>
                            <td>
                                <button class="fas fa-edit btn btn-sm btn-warning" data-bs-toggle="modal"
                                        data-bs-target="#ubah-data{{$row->id_karyawan}}">
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="tambah-data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Karyawan Baru</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <form action="{{route('createkaryawan.index')}} " method="post" role="form">
                    @csrf
                    <div class="row">
                        <div class="col-md-7">
                            <div class="card">
                                <div class="card-body">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="id_karyawan">ID Karyawan</label>
                                            <input type="text" name="id_karyawan" value="{{$kode_karyawan}}"
                                                   class="form-control" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama_karyawan">Nama Karyawan</label>
                                            <input type="text" name="nama_karyawan" class="form-control"
                                                   required>
                                        </div>
                                        <div class="form-group">
                                            <label for="tmpt_lahir">Tempat Lahir Karyawan</label>
                                            <input type="text" name="tmpt_lahir" class="form-control"
                                                   required>
                                        </div>
                                        <div class="form-group">
                                            <label for="tanggal_lahir">Tanggal Lahir</label>
                                            <input type="date" name="tggl_lahir" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="jenis_kel">Jenis Kelamin</label>
                                            <select name="jenis_kel" class="form-select form-control">
                                                <option selected disabled>Silahkan pilih</option>
                                                <option value="L">Laki-laki</option>
                                                <option value="P">Perempuan</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="alamat_kary">Alamat Karyawan</label>
                                            <input type="text" name="alamat_kary" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="status_kary">Status Karyawan</label>
                                            <select class="form-select form-control" name="status_kary" required>
                                                <option value="1">Aktif</option>
                                                <option value="0">Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="id"><strong>Id User</strong></label>
                                        <input type="text" name="id" value="{{$kode_user}}" class="form-control"
                                               readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="username"><strong>Username</strong></label>
                                        <input type="text" name="username" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Password</label>
                                        <input type="password" name="password" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="role">Role</label>
                                        <select class="form-select form-control" name="role" required>
                                            <option selected>Pilih Role User....</option>
                                            <option value="karyawan_pembelian">Karyawan Bagian Pembelian</option>
                                            <option value="karyawan_penjualan">Karyawan Bagian Penjualan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @foreach($all_karyawan as $row)
        <!--Modal Ubah -->
        <div class="modal fade text-left" id="ubah-data{{ $row->id_karyawan}}" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel1">Ubah Karyawan</h5>
                        <button type="button" class="close rounded-pill"
                                data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>
                    <form action="{{route('karyawan.update',[$row->id_karyawan])}} " method="post" role="form">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="id_karyawan">ID Karyawan</label>
                                <input type="text" name="id_karyawan" value="{{$row->id_karyawan}}"
                                       class="form-control"
                                       readonly>
                            </div>
                            <div class="form-group">
                                <label for="nama_karyawan">Nama Karyawan</label>
                                <input type="text" name="nama_karyawan" value="{{$row->nama_karyawan}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="tmpt_lahir">Tempat Lahir</label>
                                <input type="text" name="tmpt_lahir" value="{{$row->tmpt_lahir}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="tggl_lahir">Tanggal Lahir</label>
                                <input type="text" name="tggl_lahir" value="{{$row->tggl_lahir}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="jenis_kel">Jenis Kelamin</label>
                                <input type="text" name="jenis_kel" value="{{$row->jenis_kel}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="alamat_kary">Alamat Karyawan</label>
                                <input type="text" name="alamat_kary" value="{{$row->alamat_kary}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="status_kary">Status Karyawan</label>
                                <select class="form-select form-control" name="status_kary" required>
                                    <option value="1">Aktif</option>
                                    <option value="0">Non-aktif</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Simpan</button>
                            <button type="button" class="btn btn-info" data-bs-dismiss="modal">Batal</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach

@endsection
