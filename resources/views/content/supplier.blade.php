@extends('layout.main')
@section('content')
    <section class="section">
        <div class="card">
            <div class="card-header text-dark">
                <p class="text-dark bold">{{ $title }}</p>
            </div>
            <hr>
            <div class="card-body">
                <a class="btn btn-primary"data-bs-toggle="modal" data-bs-target="#tambah-data">Tambah Supplier</a>
                <a href="{{route('supplier.cetakpdf')}}" class="btn btn-primary" target="_blank">Export Pdf</a>
                <a href="{{route('supplier.Excel')}}" class="btn btn-primary" target="_blank">Export Excel</a>
                <div class="table-responsive">
                    <table class='table table-primary' id="dataTable">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Supplier</th>
                            <th>Nama Perusahaan</th>
                            <th>Alamat</th>
                            <th>Kode Pos</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($no = 1)
                        @foreach($list_supplier as $row)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$row->nama_supplier}}</td>
                                <td>{{$row->nama_perusahaan}}</td>
                                <td>{{$row->alamat_supplier}}</td>
                                <td>{{$row->kode_pos}}</td>
                                <td>{{$row->email}}</td>
                                <td>
                                    <a href="#" class="btn btn-success"
                                       data-bs-toggle="modal" data-bs-target="#edit-supplier{{$row->id}}">
                                        <i data-feather="edit"></i>Ubah</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Modal tambah -->
    <div class="modal fade text-left" id="tambah-data" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel1">Tambah Supplier</h5>
                    <button type="button" class="close rounded-pill"
                            data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <form action="{{route('supplier.add')}} " method="post" role="form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama_supplier">Nama Supplier</label>
                            <input type="text" name="nama_supplier" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="nama_perusahaan">Nama Perusahaan</label>
                            <input type="text" name="nama_perusahaan" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="alamat_supplier">Alamat Supplier</label>
                            <input type="text" name="alamat_supplier" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="kode_pos">Kode Pos</label>
                            <input type="number" name="kode_pos" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" name="email" class="form-control" required>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-info" data-bs-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @foreach($list_supplier as $row)
        <!--Modal Ubah -->
        <div class="modal fade text-left" id={{"edit-supplier". $row->id}} tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel1">Ubah Supplier</h5>
                        <button type="button" class="close rounded-pill"
                                data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>
                    <form action="{{route('supplier.update',[$row->id])}} " method="get" role="form">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="nama_supplier">Nama Supplier</label>
                                <input type="text" name="nama_supplier" value="{{$row->nama_supplier}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="nama_perusahaan">Nama Perusahaan</label>
                                <input type="text" name="nama_perusahaan" value="{{$row->nama_perusahaan}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="alamat_supplier">Alamat Supplier</label>
                                <input type="text" name="alamat_supplier" value="{{$row->alamat_supplier}}"
                                       class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="kode_pos">Kode Pos</label>
                                <input type="number" name="kode_pos" value="{{$row->kode_pos}}"
                                       class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" name="email" value="{{$row->email}}"
                                       class="form-control" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Simpan</button>
                            <button type="button" class="btn btn-info" data-bs-dismiss="modal">Batal</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection
