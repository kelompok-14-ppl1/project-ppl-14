@extends('layout.main')
@section('content')
    <div class="card">
        <div class="card-header">
            <p class="text-dark bold">{{ $title }}</p>
            <hr>
                        @if (session('success'))
                            <div class="alert-success">
                                <p>{{ session('success') }}</p>
                            </div>
                            error
                        @endif
        </div>
        <div class="card-body">
            <a class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#tambah-data">Tambah Member</a>
            <a href="{{route('member.cetakpdf')}}" class="btn btn-primary" target="_blank">Export Pdf</a>
            <a href="{{route('member.cetakExcel')}}" class="btn btn-primary" target="_blank">Export Excel</a>
            <div class="table-responsive">
                <table class="table table-primary" id="dataTable">
                    <thead class="thead-dark">
                    <tr>
                        <th>No</th>
                        <th>ID Member</th>
                        <th>Nama Member</th>
                        <th>Tempat Lahir</th>
                        <th>Tanggal lahir</th>
                        <th>Jenis Kelamin</th>
                        <th>Alamat Member</th>
                        <th>Contact Member</th>
                        <th>Status Member</th>
                        <th>Ubah Status Member</th>
                        <th>Cetak Kartu Member</th>
                        <th>Ubah Data Member</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i = 1)
                    @foreach($all_member as $row)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$row->id_member}}</td>
                            <td>{{$row->nama_member}}</td>
                            <td>{{$row->tempat_lahir}}</td>
                            <td>{{date('d-m-Y',strtotime($row->tanggal_lahir))}}</td>
                            <td>{{$row->jenis_kelamin == 'L' ? 'Laki-Laki' : 'Perempuan'}}</td>
                            <td>{{$row->alamat_member}}</td>
                            <td>{{$row->contact_member}}</td>
                            <td>{{$row->status_member == '1' ? 'Aktif' : 'Non-Aktif'}}</td>
                            <td>@if($row->status_member == '1')
                                    <a href="{{route('member.status_update',[$row->id_member])}}"
                                       class="btn btn-sm btn-success">Aktif</a>
                                @else
                                    <a href="{{route('member.status_update',[$row->id_member])}}"
                                       class="btn btn-sm btn-danger">Non-Aktif</a>

                                @endif
                            </td>
                            <td>
                                <a href="{{route('member.cetakKartuMember',[$row->id_member])}}"
                                   class="btn btn-sm btn-success">Cetak Kartu</a>
                            </td>
                            <td>
                                <button class="fas fa-edit btn btn-sm btn-warning" data-bs-toggle="modal"
                                        data-bs-target="#ubah-data{{$row->id_member}}">
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="tambah-data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Member Baru</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    </button>
                </div>

                <form action="{{route('createmember.index')}} " method="post" role="form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="id_member">Kode Member</label>
                            <input class="form-control boxed" placeholder="id_member" required="required"
                                   name="id_member" type="text" value="{{$kode_member}}" readonly>
                        </div>
                        <div class="form-group">
                            <label for="nama_member">Nama Member</label>
                            <input type="text" class="form-control" name="nama_member" required>
                        </div>
                        <div class="form-group">
                            <label for="tempat_lahir">Tempat Lahir</label>
                            <input type="text" name="tempat_lahir" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="tanggal_lahir">Tanggal Lahir</label>
                            <input type="date" name="tanggal_lahir" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="jenis_kelamin">Jenis Kelamin</label>
                            <select name="jenis_kelamin" class="form-select form-control">
                                <option selected disabled>Silahkan pilih</option>
                                <option value="L">Laki-laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="alamat_member">Alamat Member</label>
                            <input type="text" name="alamat_member" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="contact_member">Contact Member</label>
                            <input type="number" name="contact_member" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="status_member">Status Member</label>
                            <select class="form-select form-control" name="status_member" required>
                                <option value="1">Aktif</option>
                                <option value="0">Non-aktif</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-info" data-bs-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @foreach($all_member as $row)
        <!--Modal Ubah -->
        <div class="modal fade text-left" id="ubah-data{{$row->id_member}}" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel1">Ubah Member</h5>
                        <button type="button" class="close rounded-pill"
                                data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>
                    <form action="{{route('member.update',[$row->id_member])}} " method="get" role="form">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="id">ID Member</label>
                                <input type="text" name="id" value="{{$row->id_member}}" class="form-control" readonly>
                            </div>
                            <div class="form-group ">
                                <label for="nama_member">Nama Member</label>
                                <input type="text" name="nama_member" value="{{$row->nama_member}}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="tempat_lahir">Tempat Lahir</label>
                                <input type="text" name="tempat_lahir" value="{{$row->tempat_lahir}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="tanggal_lahir">Tanggal Lahir</label>
                                <input type="text" name="tanggal_lahir" value="{{$row->tanggal_lahir}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="jenis_kelamin">Jenis Kelamin</label>
                                <select class="form-select form-control" name="jenis_kelamin" required>
                                    <option value="P">P</option>
                                    <option value="L">L</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="alamat_member">Alamat Member</label>
                                <input type="text" name="alamat_member" value="{{$row->alamat_member}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="contact_member">Contact Member</label>
                                <input type="number" name="contact_member" value="{{$row->contact_member}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="status_member">Status Member</label>
                                <select class="form-select form-control" name="status_member" required>
                                    <option value="1">Aktif</option>
                                    <option value="0">Non-aktif</option>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success">Simpan</button>
                                <button type="button" class="btn btn-info" data-bs-dismiss="modal">Batal</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach

    {{--    Cetak Kartu Member--}}
    @foreach($all_member as $row)
        <!--Modal Ubah -->
        <div class="modal fade text-left" id="cetakKartuMember{{$row->id_member}}" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel1">Ubah Member</h5>
                        <button type="button" class="close rounded-pill"
                                data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>
                    <form action="{{route('member.update',[$row->id_member])}} " method="post" role="form">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="id">ID Member</label>
                                <input type="text" name="id" value="{{$row->id_member}}" class="form-control" readonly required>
                            </div>
                            <div class="form-group">
                                <label for="nama_member">Nama Member</label>
                                <input type="text" name="nama_member" value="{{$row->nama_member}}" class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="tempat_lahir">Tempat Lahir</label>
                                <input type="text" name="tempat_lahir" value="{{$row->tempat_lahir}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="tanggal_lahir">Tanggal Lahir</label>
                                <input type="text" name="tanggal_lahir" value="{{$row->tanggal_lahir}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="jenis_kelamin">Jenis Kelamin</label>
                                <input type="text" name="jenis_kelamin" value="{{$row->jenis_kelamin}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="alamat_member">Alamat Member</label>
                                <input type="text" name="alamat_member" value="{{$row->alamat_member}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="contact_member">Contact Member</label>
                                <input type="text" name="contact_member" value="{{$row->contact_member}}"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="status_member">Status Member</label>
                                <select class="form-select form-control" name="status_member" required>
                                    <option value="1">Aktif</option>
                                    <option value="0">Non-aktif</option>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success">Simpan</button>
                                <button type="button" class="btn btn-info" data-bs-dismiss="modal">Batal</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection
