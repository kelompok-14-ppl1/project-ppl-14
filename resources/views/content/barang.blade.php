@extends('layout.main')
@section('content')
    <section class="section">
        <div class="card">
            <div class="card-header text-dark">
                <p class="text-dark bold">{{ $title }}</p>
            </div>
            <hr>
            <div class="card-body">
                <a class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#tambah-data">Tambah Barang</a>
                <a href="{{route('products.cetakpdf')}}" class="btn btn-primary" target="_blank">Export Pdf</a>
                <a href="{{route('products.cetakExcel')}}" class="btn btn-primary" target="_blank">Export Excel</a>
                <div class="table-responsive">
                    <table class='table table-primary' id="dataTable">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>ID Barang</th>
                            <th>Nama Barang</th>
                            <th>Harga Barang</th>
                            <th>Stock Barang</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($i = 1)
                        @foreach($list_barang as $row)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$row->id_barang}}</td>
                                <td>{{$row->nama_barang}}</td>
                                <td><strong>Rp.</strong> {{$row->harga_barang}}</td>
                                <td>{{$row->stock_barang}}</td>
                                <td>
                                    <a href="#" class="btn btn-outline-warning" data-bs-toggle="modal"
                                       data-bs-target="#edit-product{{$row->id_barang}}"><i data-feather="edit"></i>Ubah</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--Modal tambah -->
    <div class="modal fade text-left" id="tambah-data" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel1">Tambah Barang</h5>
                    <button type="button" class="close rounded-pill"
                            data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <form action="{{route('product.create')}} " method="post" role="form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="id_barang">ID Barang</label>
                            <input type="text" name="id_barang" value="{{$kode_barang}}" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="nama_barang">Nama Barang</label>
                            <input type="text" name="nama_barang" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="harga_barang">Harga Barang (Rp)</label>
                            <input type="number" name="harga" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="stock_barang">Stock Barang</label>
                            <input type="number" name="stock_barang" class="form-control" required>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" id="save">Simpan</button>
                        <button type="button" class="btn btn-info" data-bs-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @foreach($list_barang as $row)
        <!--Modal Ubah -->
        <div class="modal fade text-left" id={{"edit-product". $row->id_barang}} tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel1">Ubah Barang</h5>
                        <button type="button" class="close rounded-pill"
                                data-bs-dismiss="modal" aria-label="Close">
                            <i data-feather="x"></i>
                        </button>
                    </div>
                    <form action="{{route('product.update',[$row->id_barang])}} " method="get" role="form">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="id_barang">ID Barang</label>
                                <input type="text" name="id" value="{{$row->id_barang}}" class="form-control" readonly>
                            </div>
                            <div class="form-group">
                                <label for="nama_barang">Nama Barang</label>
                                <input type="text" name="nama_barang" value="{{$row->nama_barang}}" class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="harga_barang">Harga Barang (Rp)</label>
                                <input type="number" name="harga" value="{{$row->harga_barang}}" class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="stock_barang">Stock Barang</label>
                                <input type="number" name="stock_barang" value="{{$row->stock_barang}}"
                                       class="form-control" required>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success" id="update">Simpan</button>
                            <button type="button" class="btn btn-info" data-bs-dismiss="modal">Batal</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach

    <script>
        $(function () {
            $('#save').on('click', function () {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Data berhasil ditambahkan!',
                    showConfirmButton: false,
                    timer: 2500
                })
            });
            $('#update').on('click', function () {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Data berhasil diubah!',
                    showConfirmButton: false,
                    timer: 2500
                })
            })
        })
    </script>
@endsection
