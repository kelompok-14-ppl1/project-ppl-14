@extends('layout.main')
@section('content')

    <div class="card" >
        <div class="card-content">
            <div class="card-header">
                <p class="text-dark bold">{{ $title }}</p>
                <hr>
                @if($message = \Illuminate\Support\Facades\Session::get('sukses'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{$message}}</strong>
                        <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @if($message = \Illuminate\Support\Facades\Session::get('gagal'))
                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                        <strong>{{$message}}</strong>
                        <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            </div>
            <div class="card-body">
{{--                <a class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#tambah-data">Tambah User</a>--}}
                <a href="{{route('user.cetakpdf')}}" class="btn btn-primary" target="_blank">Export Pdf</a>
                <a href="{{route('user.cetakexcel')}}" class="btn btn-primary" target="_blank">Export Excel</a>
                <div class="table-responsive">
                    <table class="table table-primary" id="dataTable">
                        <thead class="thead-dark">
                        <tr>
                            <th>No</th>
                            <th>ID User</th>
                            <th>Nama User</th>
                            <th>Username</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($i = 1)
                        @foreach($all_user as $row)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$row->id}}</td>
                                <td>{{$row->nama}}</td>
                                <td>{{$row->username}}</td>
                                <td>{{$row->role}}</td>
                                <td>
                                    <button class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#ubah-data{{$row->id}}">
                                        Ubah
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!--Modal tambah -->

    <div class="modal fade text-left" id="tambah-data" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel1"><strong>Tambah User</strong></h5>
                    <button type="button" class="close rounded-pill"
                            data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <form action="{{route('createuser.index')}} " method="post"  role="form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="id"><strong>Id User</strong></label>
                            <input type="text" name="id" value="{{$kode_user}}" class="form-control"
                                   readonly>
                        </div>
                        <div class="form-group">
                            <label for="nama_user"><strong>Nama User</strong></label>
                            <input type="text"  name="nama" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="username"><strong>Username</strong></label>
                            <input type="text"  name="username" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="username">Password</label>
                            <input type="password"  name="password" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="role">Role</label>
                            <select class="form-select form-control" name="role" required>
                                <option selected>Pilih Role User....</option>
                                <option value="admin">Admin</option>
                                <option value="karyawan_penjualan">Karyawan Bagian Pembelian</option>
                                <option value="karyawan_pembelian">Karyawan Bagian Penjualan</option>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-info" data-bs-dismiss="modal">Batal</button>

                    </div>

                </form>

            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="tambah-data1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah User Baru</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">

                    </button>
                </div>
                <form action="{{route('createuser.index')}} " method="post"  role="form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama_user">Nama User</label>
                            <input type="text"  name="nama" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text"  name="username" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="username">Password</label>
                            <input type="password"  name="password" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="role">Role</label>
                            <select required name="role" id="" class="form-control">
                                <option value="" selected disabled>Pilih Role User</option>
                                <option value="admin">Admin</option>
                                <option value="karyawan_penjualan">Karyawan Bagian Pembelian</option>
                                <option value="karyawan_pembelian">Karyawan Bagian Penjualan</option>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-info" data-bs-dismiss="modal">Batal</button>

                    </div>

                </form>

            </div>
        </div>
    </div>

    <!-- Modal -->
    @foreach($all_user as $row)

    <div class="modal fade" id="ubah-data{{$row->id}}"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>Tambah User Baru</strong></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">

                    </button>
                </div>

                <form action=" {{route('user.update',[$row->id])}}" method="get"  role="form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama_user"><strong>Nama User</strong></label>
                            <input type="text"  name="nama" value="{{$row->nama}}" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="username"><strong>Username</strong></label>
                            <input type="text"  name="username" class="form-control" value="{{$row->username}}" required>
                        </div>
                        <div class="form-group">
                            <label for="username"><strong>Password</strong></label>
                            <input type="text"  name="password" class="form-control" value="{{$row->password}}" readonly>

                        </div>
                        <div class="form-group">
                            <label for="role"><strong>Role</strong></label>
                            <input type="text"  name="role" class="form-control" value="{{$row->role}}" readonly>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-info" data-bs-dismiss="modal">Batal</button>

                    </div>

                </form>

            </div>
        </div>
    </div>

    @endforeach

@endsection
