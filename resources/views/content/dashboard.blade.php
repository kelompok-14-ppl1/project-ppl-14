@extends('layout.main')
@section('content')
    <section class="section">
        <div class="row mb-2">
            <div class="col-12 col-md-3">
                <div class="card card-statistic">
                    <div class="card-body p-0">
                        <div class="d-flex flex-column">
                            <div class='px-3 py-3 d-flex justify-content-between'>
                                <h3 class='card-title'>User</h3>
                                <div class="card-right d-flex align-items-center">
                                    <h1 class="text-dark text-4xl">{{$jumlahUser}}</h1>
                                </div>
                            </div>
                            <div class="icon">
                                <p></p>
                                <i class="fa fa-user fa-3x white"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="card card-statistic">
                    <div class="card-body p-0">
                        <div class="d-flex flex-column">
                            <div class='px-3 py-3 d-flex justify-content-between'>
                                <h3 class='card-title'>Karyawan</h3>
                                <div class="card-right d-flex align-items-center">
                                    <h1>{{$jumlahKaryawan}}</h1>
                                </div>
                            </div>
                        </div>
                        <div class="icon">
                            <p></p>
                            <i class="fa fa-users fa-3x white"></i>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="card card-statistic">
                    <div class="card-body p-0">
                        <div class="d-flex flex-column">
                            <div class='px-3 py-3 d-flex justify-content-between'>
                                <h3 class="card-title">Member</h3>
                                <div class="card-right d-flex align-items-center">
                                    <h1>{{$jumlahMember}}</h1>
                                </div>
                            </div>
                            <div class="icon">
                                <p></p>
                                <i class="fa fa-users fa-3x white"></i>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="card card-statistic">
                    <div class="card-body p-0">
                        <div class="d-flex flex-column">
                            <div class='px-3 py-3 d-flex justify-content-between'>
                                <h3 class='card-title'>Barang</h3>
                                <div class="card-right d-flex align-items-center">
                                    <h1>{{$jumlahBarang}}</h1>
                                </div>
                            </div>
                            <div class="icon">
                                <p></p>
                                <i class="fa fa-boxes fa-3x white"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="card card-statistic">
                    <div class="card-body p-0">
                        <div class="d-flex flex-column">
                            <div class='px-3 py-3 d-flex justify-content-between'>
                                <h3 class='card-title'>Supplier</h3>
                                <div class="card-right d-flex align-items-center">
                                    <h1>{{$jumlahSupplier}}</h1>
                                </div>
                            </div>
                            <div class="icon">
                                <p></p>
                                <i class="fa fa-truck-fast fa-3x white"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="card card-statistic">
                    <div class="card-body p-0">
                        <div class="d-flex flex-column">
                            <div class='px-3 py-3 d-flex justify-content-between'>
                                <h3 class="text-white">Transaksi Pembelian</h3>
                                <div class="card-right d-flex align-items-center">
                                    <h1>{{$jumlahTrxPembelian}}</h1>
                                </div>
                            </div>
                            <div class="icon">
                                <p></p>
                                <i class="fa fa-shop fa-3x white"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="card card-statistic">
                    <div class="card-body p-0">
                        <div class="d-flex flex-column">
                            <div class='px-3 py-3 d-flex justify-content-between'>
                                <h3 class="text-white">Transaksi Penjualan</h3>
                                <div class="card-right d-flex align-items-center">
                                    <h1>{{$jumlahTrxPenjualan}}</h1>
                                </div>
                            </div>
                            <div class="icon">
                                <p></p>
                                <i class="fa fa-shop fa-3x white"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


@endsection
