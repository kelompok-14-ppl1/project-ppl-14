@extends('layout.main')
@section('content')
    <section class="section">
        <div class="card">
            <div class="card-header text-dark">
                <p class="text-dark bold">{{ $title }}</p>
            </div>
            <hr>
            <div class="card-body">
                <a href="{{route('transaksi.pembelian.add')}}" class="btn btn-simple btn-info">Tambah Transaksi</a>
                <a href="{{route('trxPembelian.cetakpdf')}}" class="btn btn-primary" target="_blank">Export Pdf</a>
                <a href="{{route('trxPembelian.cetakExcel')}}" class="btn btn-primary" target="_blank">Export Excel</a>
                <div class="table-responsive">
                    <table class="table table-striped table-info" id="dataTable">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>No Transaksi</th>
                            <th>Tanggal Pembelian</th>
                            <th>ID Karyawan</th>
                            <th>Detail Transaksi</th>
                            <th>Cetak Invoice Transaksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($i = 1)
                        @foreach($list_transaksi as $row)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$row->nomor_transaksi}}</td>
                                <td>{{$row->tanggal_pembelian}}</td>
                                <td>{{$row->id_karyawan}}</td>
                                <td>
                                    <a href="{{route('detail.trans.pembelian',[$row->nomor_transaksi])}}" class="btn btn-sm btn-primary"><i data-feather="eye"></i> Detail</a>
                                </td>
                                <td>
                                    <a href="{{route('trans.cetakinvoice',[$row->nomor_transaksi])}}" class="btn btn-sm btn-success"> <i data-feather="eye"></i> Cetak Invoice Transaksi</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection

detail.trans
