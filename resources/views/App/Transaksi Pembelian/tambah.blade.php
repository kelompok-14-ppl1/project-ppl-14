@extends('layout.main')
@section('content')
    <div class="row">
        <div class="col-4">
            <div class="card">
                <div class="card-header">
                    <p class="text-dark bold">Cari Barang</p>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="search-id-barang">Kode Barang</label>
                        <input id="search-id-barang" type="text" class="form-control" autofocus>
                    </div>
                    <div class="form-group">
                        <label for="jumlah-barang">Jumlah Barang</label>
                        <input value="1" type="number" id="jumlah-barang" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nama-barang">Nama Barang</label>
                        <input readonly disabled type="text" id="nama-barang" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="harga-barang">Harga Barang</label>
                        <input readonly disabled type="number" id="harga-barang" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="stock-barang">Stock Barang</label>
                        <input readonly disabled type="number" id="stock-barang" class="form-control">
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary" id="addProduct" style="float: right">
                        <i data-feather="plus-square"></i>
                        <span>Tambah Barang</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div class="card">
                <div class="card-header">
                    <p class="text-dark bold">Daftar Belanjaan</p>
                </div>
                <div class="card-body">
                    <div class="form-grup">
                        <label for="supplier">Pilih Supplier</label>
                        <select id="supplier-name" class="form-select form-select-sm"
                                aria-label=".form-select-sm example">
                            <option selected>Silahkan Pilih Supplier</option>
                            @foreach($list_supplier as $row)
                                <option value="{{$row->id}}">{{$row->nama_perusahaan}}</option>
                            @endforeach
                        </select>
                        <hr>
                        <div class="table-responsive">
                            <table class="table table-striped table-info" id="table-belanja">
                                <thead>
                                <tr>
                                    <th>Nama Barang</th>
                                    <th>Harga Barang</th>
                                    <th>Jumlah Barang</th>
                                    <th>Total Belanja</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <div class="col-9">
                            <div class="form-grup">
                                <label for="total_belanja" class="font-bold">Total Belanjaan</label>
                                <input type="text" id="total_belanjaan" class="form-control text-center font-bold"
                                       style="padding: 30px; font-size: 25px;" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-success" id="save_transaksi" style="float: right">
                        <i data-feather="save"></i>
                        <span>Simpan</span>
                    </button>
                    <a href="{{route('transaksi.pembelian.index')}}" class="btn btn-danger"
                       style="float: left">
                        <i data-feather="x"></i>
                        <span>Batal</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('#addProduct').prop("disabled", true);

            $('#search-id-barang').on('keypress', function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    $.LoadingOverlay('show');
                    let idBarangDariInput = $(this).val();
                    $.ajax({
                        url: '{{route("ajax.getBarangByKode")}}',
                        type: 'POST',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            idbarang: idBarangDariInput,
                        },
                        success: function (result) {
                            $('#nama-barang').val(result.nama_barang);
                            $('#harga-barang').val(result.harga_barang);
                            $('#stock-barang').val(result.stock_barang);
                            $.LoadingOverlay('hide');
                            $('#nama-barang').trigger("change");
                        },
                        error: function (jqXHR, error, errorThrown) {
                            $.LoadingOverlay('hide');
                            $('#nama-barang').val('');
                            $('#harga-barang').val('');
                            $('#stock-barang').val('');
                            resetForm();
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Kode barang yang anda cari tidak ditemukan!',
                            });
                        }
                    });
                }
            });

            function clearForm() {
                $('#nama-barang').val('');
                $('#harga-barang').val('');
                $("#search-id-barang").val('');
                $("#jumlah-barang").val('1')
            }

            $('#addProduct').on('click', function () {
                let jumlahBarang = $("#jumlah-barang").val();
                let namaBarang = $("#nama-barang").val();
                let hargaBarang = $('#harga-barang').val();
                let kodeBarang = $("#search-id-barang").val();
                let totalBelanja = TotalBelanja();
                console.log(jumlahBarang);
                //insert ke Daftar belanjaan
                let tableData = `<tr data-id-barang="${kodeBarang}">`;
                tableData += `<td class="text-right">${namaBarang}</td>`
                tableData += `<td class="text-center">${hargaBarang}</td>`
                tableData += `<td class="text-left">${jumlahBarang}</td>`
                tableData += `<td class="total-belanja text-left">${totalBelanja}</td>`
                tableData += `<td><button class="btn-hapus-item btn btn-outline-danger btn-xs">
                               <i class="far fa-minus-square"></i></button></td>`
                tableData += '</tr>';

                $('#table-belanja tbody').append(tableData);

                clearForm();
                $('#nama-barang').trigger("change");
                inisialisasiBtnHapus();
                hitungTotalBelanjaan();
            });


            function TotalBelanja() {
                let jumlahBarang = parseInt($("#jumlah-barang").val());
                let hargaBarang = parseInt($("#harga-barang").val());
                let total = jumlahBarang * hargaBarang;
                return parseInt(total);
            }

            /**
             * Fungsi inisialisasi tombol hapus
             */
            function inisialisasiBtnHapus() {
                /**
                 * Event on click pada btn hapus item
                 */
                $('.btn-hapus-item').on('click', function () {
                    $(this).parent().parent().remove();
                    hitungTotalBelanjaan();
                });
            }

            function hitungTotalBelanjaan() {
                let totalBelanja = $('.total-belanja');
                let total = 0;
                totalBelanja.each(function (index, value) {
                    let subTotal = totalBelanja.eq(index);
                    total += parseInt(subTotal.text());
                });
                $("#total_belanjaan").val(total);
            }

            function resetForm() {
                $('#search-id-barang').val('');
                $('#jumlah-barang').val('1');
            }

            $('#nama-barang').on("change", function () {
                let isiNamaBarang = $(this).val();
                if (isiNamaBarang === "") {
                    $("#addProduct").prop("disabled", true);
                } else {
                    $("#addProduct").prop("disabled", false);
                }
            });

            /**
             * Proses simpan Transaksi Pembelian
             */
            $('#save_transaksi').on('click', function () {
                let detailTransaksi = [];
                let suppliers = $('#supplier-name').val();
                let dataTableBelanjaan = $('#table-belanja tbody').children();
                dataTableBelanjaan.each(function (index, value) {
                    let item = dataTableBelanjaan.eq(index);
                    let itemTransaksi = {
                        itrx_nama_barang: item.children().eq(0).text(),
                        itrx_harga_barang: item.children().eq(1).text(),
                        itrx_jumlah_barang: item.children().eq(2).text(),
                        itrx_total_pembelian: item.children().eq(3).text(),
                        itrx_id_barang: item.data("id-barang"),
                    }
                    detailTransaksi.push(itemTransaksi);
                });
                $.ajax({
                    url: '{{route("transaksi.pembelian.proses")}}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        item_transaksi: detailTransaksi,
                        supplier: suppliers,
                    },
                    success: function (result) {
                        window.location = '{{route("transaksi.pembelian.index")}}'
                    }
                });
            });
        })
    </script>
@endsection
