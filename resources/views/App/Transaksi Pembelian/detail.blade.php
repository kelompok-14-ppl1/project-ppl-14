
@extends('layout.main')
@section('content')
    <div class="card">
        <div class="card-header">
            <a href="{{route('transaksi.pembelian.index')}}" class="btn btn-sm btn-success">Kembali</a>
            <h1></h1>
            <p class="text-dark bold">{{ $title }}</p>
            <div class="col text-center">
                <h2 class="text-bold">SITOKER</h2>
                <p>No Hp  : xxx8376</p>
                <p>Alamat : Jalan Kenangan No.1 </p>
                <p></p>
                <br>
                <h5><strong>{{$title}}</strong></h5>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <td class="text-bold text-dark">No Transaksi</td>
                        <td class="text-bold text-dark">Tanggal Transaksi</td>
                        <td class="text-bold text-dark">Nama Supplier</td>
                    </tr>
                    @foreach($transaksi as $row)
                        <tr>
                            <td>{{$row->nomor_transaksi}}</td>
                            <td>{{$row->tanggal_pembelian}}</td>
                            <td>{{$row->nama_supplier}}</td>
                        </tr>
                    @endforeach
                </table>
                <h1></h1>
                <table class="table table-striped">
                    <tr>
                        <td>No</td>
                        <td>Nama Barang</td>
                        <td>Harga Barang</td>
                        <td>Jumlah</td>
                        <td>Sub Total Harga Barang</td>
                    </tr>
                    @php($i = 1)
                    @foreach($detailTransaksi as $row)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$row->itrx_nama_barang}}</td>
                            <td>Rp. {{number_format($row->itrx_harga_barang,'0',',','.')}}</td>
                            <td class="text-center">{{$row->itrx_jumlah_barang}}</td>
                            <td>Rp. {{number_format($row->itrx_total_pembelian, '0', ',','.')}}</td>
                        </tr>
                        <tfoot>
                        <tr>
                            <td class="right text-bold text-dark">Total Belanja Rp.{{number_format($row->itrx_total_pembelian, '0',',','.')}}</td>
                        </tr>
                        </tfoot>
                    @endforeach

                </table>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col text-center">
                    <h6 class="h6 text-bold">Terimakasih Telah Berbelanja</h6>
                    <p class="small">Barang Yang Sudah Dibeli Tidak Dapat Dikembalikan Lagi</p>
                </div>
            </div>
        </div>
    </div>
@endsection
