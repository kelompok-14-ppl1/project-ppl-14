
@extends('layout.main')
@section('content')
    <div class="card">
        <div class="card-header">
            <p class="text-dark bold">{{ $title }}</p>
        </div>
        <div class="card-body">
            <a href="{{route('app.trans')}}" class="btn btn-primary">
                <i class="fa fa-cart-plus fa-sm"></i>
                Tambah Transaksi
            </a>
            <a href="{{route('trxPenjualan.cetakpdf')}}" class="btn btn-primary" target="_blank">Export Pdf</a>
            <a href="{{route('trxPenjualan.cetakExcel')}}" class="btn btn-primary" target="_blank">Export Excel</a>

            <div class="table-responsive">
                <table class="table table-striped table-light" id="dataTable">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Nomor Transaksi</th>
                        <th>Tanggal Transaksi</th>
                        <th >Id Member</th>
                        <th>Id Karyawan</th>
                        <th>DetailTransaksi</th>
                        <th>Cetak Invoice Transaksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($trans as $row)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$row->id_transaksi_penjualan}}</td>
                            <td>{{$row->tanggal_transaksi_penjualan}}</td>
                            <td>{{$row->id_member_trans}}</td>
                            <td>{{$row->id_karyawan_penjualan}}</td>
                            <td>
                                <a href="{{route('detail.trans.penjualan',[$row->id_transaksi_penjualan])}}" class="btn btn-sm btn-primary"><i data-feather="eye"></i> Detail</a>
                            </td>
                            <td>
                                <a href="{{route('trans.cetakinvoice',[$row->id_transaksi_penjualan])}}" class="btn btn-sm btn-success"> <i data-feather="eye"></i> Cetak Invoice Transaksi</a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer"></div>
    </div>
@endsection
