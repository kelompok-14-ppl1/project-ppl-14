@extends('layout.main')
@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <p class="text-dark bold">Cari Barang</p>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Kode Barang</label>
                        <input id="search-id-barang" type="text" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Jumlah Barang</label>
                        <input id="jumlah-barang" value="1" type="number" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Nama Barang</label>
                        <input id="nama-barang" readonly disabled type="text " class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Harga Barang</label>
                        <input id="harga-barang" readonly disabled type="number" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Stock Barang</label>
                        <input id="stock-barang" readonly disabled type="number" class="form-control"/>
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-outline-primary" id="addProduct" style="float: right">
                        <i data-feather="plus-square"></i>
                        <span>Simpan</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <p class="text-dark bold">List Barang</p>
                </div>
                <div class="card-body">
                    <div class="form-grup">
                        <hr>
                        <table class="table table-striped table-info" id="table-penjualan">
                            <thead>
                            <tr>
                                <th>Nama Barang</th>
                                <th>Harga Barang</th>
                                <th>Jumlah Barang</th>
                                <th>Total Belanja</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div class="row">
                            <div style="margin-top: 50px;" class="offset-8 col-md-4">
                                <div class="form-group">
                                    <label for="">Total Belanja</label>
                                    <input class="form-control text-right" id="total_belanjaan" readonly type="text"
                                           placeholder="Total Belanja" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="offset-8 col-md-4">
                                <div class="form-group">
                                    <label for="">Member</label>
                                    <input class="form-control text-right" id="kode-member" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="offset-8 col-md-4">
                                <div class="form-group">
                                    <label for="">Diskon</label>
                                    <input class="form-control text-right" id="diskon-member" type="number"
                                           disabled value="0"
                                           required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="offset-8 col-md-4">
                                <div class="form-group">
                                    <label for="">Bayar(Rp)</label>
                                    <input class="form-control" id="total-bayar" type="number" placeholder="Bayar"
                                           required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="offset-8 col-md-4">
                                <div class="form-group">
                                    <label for="">Kembalian</label>
                                    <input class="form-control text-right" id="kembalian" readonly type="number"
                                           placeholder="Kembalian" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" id="save_penjualan" class="btn btn-success">
                        <i data-feather="save"></i> Save
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            $('#addProduct').prop("disabled", true);

            $('#search-id-barang').on('keypress', function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    $.LoadingOverlay('show');
                    let idBarangDariInput = $(this).val();
                    $.ajax({
                        url: '{{route("ajax.getBarangByKode")}}',
                        type: 'POST',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            idbarang: idBarangDariInput,
                        },
                        success: function (result) {
                            $('#nama-barang').val(result.nama_barang);
                            $('#harga-barang').val(result.harga_barang);
                            $('#stock-barang').val(result.stock_barang);
                            $.LoadingOverlay('hide');
                            $('#nama-barang').trigger("change")
                        },
                        error: function (jqXHR, error, errorThrown) {
                            $.LoadingOverlay('hide');
                            $('#nama-barang').val('');
                            $('#harga-barang').val('');
                            $('#jumlah-barang').val('1');
                            Swal.fire({
                                text: 'Barang Tidak Ditemukan!!',
                                icon: 'error'
                            });
                        }
                    });
                }
            });

            function clearForm() {
                $('#nama-barang').val('');
                $('#harga-barang').val('');
                $('#stock-barang').val('');
                $("#search-id-barang").val('');
                $("#jumlah-barang").val('1')
            }

            $('#addProduct').on('click', function () {
                let jumlahBarang = $("#jumlah-barang").val();
                let namaBarang = $("#nama-barang").val();
                let hargaBarang = $('#harga-barang').val();
                let kodeBarang = $("#search-id-barang").val();
                let totalBelanja = TotalBelanja();
                let stockBarang = $("#stock-barang").val();
                console.log(jumlahBarang);
                if (parseInt(jumlahBarang) > parseInt(stockBarang)) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Stock barang tidak mencukupi!!!',
                        showConfirmButton: false,
                        timer: 2000
                    })
                } else {
                    //insert ke Daftar belanjaan
                    let tableData = `<tr data-id-barang="${kodeBarang}">`;
                    tableData += `<td class="text-right">${namaBarang}</td>`
                    tableData += `<td class="text-center">${hargaBarang}</td>`
                    tableData += `<td class="text-left">${jumlahBarang}</td>`
                    tableData += `<td class="total-belanja text-left">${totalBelanja}</td>`
                    tableData += `<td><button class="btn-hapus-item btn btn-outline-danger btn-xs">
                               <i class="far fa-minus-square"></i></button></td>`
                    tableData += '</tr>';

                    $('#table-penjualan tbody').append(tableData);

                    //Minus stock
                    $("#search-id-barang").on('keypress', function () {
                        let idBarang = $(this).val();
                        $.ajax({
                            url: '{{route("app.trans")}}',
                            success: function (result) {
                                if (kodeBarang === idBarang) {
                                    let stock = stockBarang - jumlahBarang
                                    $("#stock-barang").val(stock);
                                }
                            }
                        });
                    });

                    clearForm();
                    $('#nama-barang').trigger("change");
                    inisialisasiBtnHapus();
                    hitungTotalBelanjaan();
                }
            });

            // Hitung uang kembalian
            $('#total-bayar').on('keyup', function () {
                let totalBayar = ($(this).val());
                console.log(totalBayar);
                $.ajax({
                    // Pertama Set URL nya set method, set dataType
                    url: '{{route("app.trans")}}',
                    success: function () {
                        let totalBelanja = $("#total_belanjaan").val();
                        let diskon = $("#diskon-member").val();
                        console.log(totalBelanja);
                        if (totalBayar.length >= totalBelanja.length) {
                            let totalKembalian = totalBayar - (totalBelanja - diskon);
                            console.log(totalKembalian);
                            $("#kembalian").val(totalKembalian);
                        } else {
                            $("#kembalian").val('');
                        }
                    }
                });
            });

            // Find kode member
            $('#kode-member').on('keypress', function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    $.LoadingOverlay('show');
                    let kodeMemberDariInput = $(this).val();
                    let totalBelanja = $("#total_belanjaan").val();
                    let diskon = totalBelanja * 0.10;
                    $.ajax({
                        url: '{{route("ajax.getKodeMember")}}',
                        type: 'POST',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            kodeMember: kodeMemberDariInput,
                        },
                        success: function (result) {
                            $.LoadingOverlay('hide');
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Kode member ditemukan!',
                                showConfirmButton: false,
                                timer: 1500
                            })
                            $("#diskon-member").val(diskon);
                        },
                        error: function (jqXHR, error, errorThrown) {
                            $.LoadingOverlay('hide');
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Member tidak terdaftar!',
                                showConfirmButton: false,
                                timer: 2000
                            })
                        }
                    });
                }
            });

            function TotalBelanja() {
                let jumlahBarang = parseInt($("#jumlah-barang").val());
                let hargaBarang = parseInt($("#harga-barang").val());
                let total = jumlahBarang * hargaBarang;
                return parseInt(total);
            }

            /**
             * Fungsi inisialisasi tombol hapus
             */
            function inisialisasiBtnHapus() {
                /**
                 * Event on click pada btn hapus item
                 */
                $('.btn-hapus-item').on('click', function () {
                    $(this).parent().parent().remove();
                    hitungTotalBelanjaan();
                    $("#diskon-member").val('0');
                    $("#kode-member").val('');
                });
            }

            function hitungTotalBelanjaan() {
                let totalBelanja = $('.total-belanja');
                let total = 0;
                totalBelanja.each(function (index, value) {
                    let subTotal = totalBelanja.eq(index);
                    total += parseInt(subTotal.text());
                });
                $("#total_belanjaan").val(total);
            }

            function resetForm() {
                $('#search-id-barang').val('');
                $('#jumlah-barang').val('1');
            }

            $('#nama-barang').on("change", function () {
                let isiNamaBarang = $(this).val();
                if (isiNamaBarang === "") {
                    $("#addProduct").prop("disabled", true);
                } else {
                    $("#addProduct").prop("disabled", false);
                }
            });

            $('#save_penjualan').on('click', function () {
                let salesDetail = [];
                let potongan = $('#diskon-member').val();
                let idMember = $('#kode-member').val();
                let dataTablePenjualan = $('#table-penjualan tbody').children();
                dataTablePenjualan.each(function (index, value) {
                    let item = dataTablePenjualan.eq(index);
                    let salesItem = {
                        nama_barang: item.children().eq(0).text(),
                        harga_barang: item.children().eq(1).text(),
                        jumlah_barang: item.children().eq(2).text(),
                        total_harga_penjualan: item.children().eq(3).text(),
                        id_barang: item.data("id-barang"),
                    }
                    salesDetail.push(salesItem);
                });
                $.ajax({
                    url: '{{route("app.proses.trans")}}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        sales_item_transaksi: salesDetail,
                        discount: potongan,
                        kodeMember: idMember,
                    },
                    success: function (result) {
                        window.location = '{{route("trans_jual.index")}}';
                    }
                })
            })
        });
    </script>
@endsection
