@extends('layout.main')
@section('content')
    <div class="card">
        <div class="card-header">
            <a href="{{route('trans_jual.index')}}" class="btn btn-sm btn-success">Kembali</a>
            <h1></h1>
            <p class="text-dark bold">{{ $title }}</p>
            <div class="col text-center">
                <h2 class="text-bold">SITOKER</h2>
                <p>No Hp : xxx8376</p>
                <p>Alamat : Jalan Kenangan No.1 </p>
                <p></p>
                <br>
                <h5><strong>{{$title}}</strong></h5>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <td class="text-bold text-dark">No Transaksi</td>
                        <td class="text-bold text-dark">Tanggal Transaksi</td>
                        <td class="text-bold text-dark">Nama Member</td>
                        <td class="text-bold text-dark">Nama Kasir</td>
                        <td class="text-bold text-dark">Discount</td>
                    </tr>
                    @foreach($transaksi as $row)
                        <tr>
                            <td>{{$row->id_transaksi_penjualan}}</td>
                            <td>{{$row->tanggal_transaksi_penjualan}}</td>
                            <td>{{$row->nama_member}}</td>
                            <td>{{$row->nama_karyawan}}</td>
                            <td>{{number_format($row->diskon,'0', ',','.')}}</td>
                        </tr>
                    @endforeach
                </table>
                <h1></h1>
                <table class="table table-striped">
                    <tr>
                        <td>No</td>
                        <td>Nama Barang</td>
                        <td>Harga Barang</td>
                        <td>Jumlah</td>
                        <td>Sub Total Harga Barang</td>
                    </tr>
                    @php($i = 1)
                    @foreach($detailTransaksi as $row)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$row->nama_barang}}</td>
                            <td>{{number_format($row->harga_barang, '0', ',','.')}}</td>
                            <td>{{$row->jumlah_barang}}</td>
                            <td>{{number_format($row->total_harga_penjualan, '0', ',','.')}}</td>
                        </tr>

                        <tfoot>
                        <tr>
                            @foreach($transaksi as $baris)
                                <td class="right text-bold">Total
                                    Belanja {{number_format($row->total_harga_penjualan - $baris->diskon, 0,',','.')}}</td>
                            @endforeach
                        </tr>
                        </tfoot>
                    @endforeach

                </table>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col text-center">
                    <h6 class="h6 text-bold">Terimakasih Telah Berbelanja</h6>
                    <p class="small">Barang Yang Sudah Dibeli Tidak Dapat Dikembalikan Lagi</p>
                </div>
            </div>
        </div>
    </div>
@endsection
