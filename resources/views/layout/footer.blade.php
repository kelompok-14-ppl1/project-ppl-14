<script src="https://kit.fontawesome.com/d4bd07badb.js" crossorigin="anonymous"></script>

<script src="{{URL::asset('assets/js/feather-icons/feather.min.js')}}"></script>
<script src="{{URL::asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{URL::asset('assets/js/app.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js">
</script>

<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>

{{--Datatables--}}
<script src="{{URL::asset('assets/vendors/simple-datatables/simple-datatables.js')}}"></script>
<script src="{{URL::asset('assets/js/vendors.js')}}"></script>
<script src="{{URL::asset('assets/js/main.js')}}"></script>

<script src="{{URL::asset('assets/vendors/chartjs/Chart.min.js')}}"></script>
<script src="{{URL::asset('assets/vendors/apexcharts/apexcharts.min.js')}}"></script>
<script src="{{URL::asset('assets/js/pages/dashboard.js')}}"></script>

{{--Sweet alert--}}
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
{{--<script src="{{URL::asset('assets/vendors/js/demo/datatables-demo.js')}}"></script>--}}
{{--<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>--}}
{{--<script src="htwtps://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>--}}
