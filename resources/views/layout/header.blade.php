<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<link rel="icon" type="image/png" href="{{URL::asset('assets_login/img/android-icon-72x72.png')}}">
<title>SITOKER - {{$title}}</title>
<link rel="stylesheet" href="{{URL::asset('assets/vendors/chartjs/Chart.min.css')}}">

<link rel="stylesheet" href="{{URL::asset('assets/css/bootstrap.css')}}">

{{--FOnt Awesome--}}
<link rel="stylesheet" href="{{URL::asset('assets/fontawesome/css/all.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('assets/fontawesome/js/all.min.js')}}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
      integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">


<link rel="stylesheet" href="{{URL::asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.css')}}">

<link rel="stylesheet" href="{{URL::asset('assets/vendors/simple-datatables/style.css')}}">


<link rel="stylesheet" href="{{URL::asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.css')}}">
<link rel="stylesheet" href="{{URL::asset('assets/css/app.css')}}">
<link rel="shortcut icon" href="{{URL::asset('assets/images/favicon.svg" type="image/x-icon')}}">

{{-- Jquery --}}
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
{{--//datatables--}}
{{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">--}}
