<a class="sidebar-toggler" href="#"><span class="navbar-toggler-icon"></span></a>
<button class="btn navbar-toggler" type="button" data-bs-toggle="collapse"
        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav d-flex align-items-center navbar-light ms-auto">
        <li class="dropdown">
            <a href="#" data-bs-toggle="dropdown"
               class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                <div class="d-none d-md-block d-lg-inline-block">Hi {{ session('namaUserLogin') }}</div>
                <nbsp></nbsp>
                <div class="avatar me-1">
                    <img src="{{URL::asset('assets/images/avatar/avatar-s-1.png')}}"
                         alt="" srcset="">
                </div>
{{--                <div class="d-none d-md-block d-lg-inline-block">Hi, Three</div>--}}
            </a>
            <div class="dropdown-menu dropdown-menu-end">
                <a class="dropdown-item"><strong>Hi {{ session('namaUserLogin') }}</strong></a>
                <a class="btn dropdown-item" data-bs-toggle="modal" data-bs-target="#ubah-password"><i data-feather="user"></i>Ubah Password</a>
                <a class="btn dropdown-item" data-bs-toggle="modal" data-bs-target="#infoUserLogin"><i data-feather="settings"></i>Detail Login</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{route('logout.index')}}  "><i data-feather="log-out"></i> Logout</a>
            </div>
        </li>
    </ul>
</div>


<!-- Modal Ubah Password -->
<div class="modal fade" id="ubah-password" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ubah Password</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">

                </button>
            </div>
            <form action="{{route('user.ubahpassword')}} " method="get"  role="form">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="password_lama">Password Lama</label>
                        <input type="text"  name="password_lama" class="form-control" placeholder="Inputkan Password Lama Anda" required>
                    </div>
                    <div class="form-group">
                        <label for="username">Password Baru</label>
                        <input type="text"  name="password" class="form-control"  placeholder="Inputkan Password Baru Anda" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-info" data-bs-dismiss="modal">Batal</button>
                </div>

            </form>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="infoUserLogin" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-dark" id="exampleModalLabel">Detail Login Anda</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                </button>
            </div>
                <div class="modal-body">
                    <h5 class="text-dark" >Sekarang Tanggal {{date('d F Y')}}</h5>
                    <h1></h1>
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" value=" {{session('namaUserLogin')}}" class="form-control" readonly>
                    </div>
                    <div class="form-group">
                        <label for="password">Username</label>
                        <input type="text" value=" {{session('usernameUserLogin')}}"  class="form-control" readonly>
                    </div>
                    <div class="form-group">
                        <label for="username">Role</label>
                        <input type="text" value=" {{session('roleUserLogin')}}"  class="form-control" readonly>
                    </div>
                    <div class="form-group">
                        <label for="username">Jam Login</label>
                        <input type="text" value=" {{session('jamLogin')}} WIB"   class="form-control" readonly>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-bs-dismiss="modal">Close</button>

                </div>
        </div>
    </div>
</div>

