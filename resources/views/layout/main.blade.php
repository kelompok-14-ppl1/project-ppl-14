<!DOCTYPE html>
{{-- cdn bootstrap  --}}
<html lang="en">
<head>
    @include('layout.header')
</head>
<body>
<div id="app">
    <div id="sidebar" class='active'>
        <div class="sidebar-wrapper active">
            @include('layout.sidebar')
        </div>
    </div>

    <div id="main">
        <nav class="navbar navbar-header navbar-expand navbar-light">
            @include('layout.topbar')
        </nav>

        <div class="main-content container-fluid">
            <div class="page-title">
                <h3>{{$title}}</h3>
                <p class="text-subtitle text-muted">Aplikasi Toko Retail Kelompok 14</p>

                <section class="section">
                    @yield('content')
                </section>
            </div>
        </div>

        <footer>
            <div class="footer clearfix mb-0 text-muted">
                <div class="float-start">
                    <p>2021 &copy; Toko Retail 14</p>
                </div>
                <div class="float-end">
                    <p>Crafted with <span class='text-danger'><i data-feather="heart"></i></span> by Kelompok 14 PPL</p>
                </div>
            </div>
        </footer>
    </div>

    @include('layout.footer')
</div>

</body>

</html>
