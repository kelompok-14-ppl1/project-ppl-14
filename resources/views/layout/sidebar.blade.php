<div class="sidebar-header">
    <img src="{{URL::asset('assets/images/logo 1.jpg')}}" alt="" srcset="">
    <p>Toko Retail</p>
</div>
<div class="sidebar-menu">
    <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
    <ul class="menu">

        <li class='sidebar-title'>Main Menu</li>

        <li class="sidebar-item active ">
            <a href="{{route('dashboard.index')}}" class='sidebar-link'>
                <i data-feather="home" width="20"></i>
                <span>Dashboard</span>
            </a>
        </li>

        <li class='sidebar-title'>Data Master</li>

        @if(session('roleUserLogin') === 'pemilik_toko' && 'admin')
            <li class="sidebar-item">
                <a href="{{route('user.index')}}" class='sidebar-link'>
                    <i data-feather="user" width="20">></i>
                    <span>User</span>
                </a>
            </li>

            <li class="sidebar-item">
                <a href="{{route('karyawan.index')}}" class='sidebar-link'>
                    <i data-feather="users" width="20">></i>
                    <span>Karyawan</span>
                </a>
            </li>

        @endif


        <li class="sidebar-item">
            <a href="{{route('member.index')}}" class='sidebar-link'>
                <i data-feather="users" width="20">></i>
                <span>Member</span>
            </a>
        </li>


        <li class="sidebar-item">
            <a href="{{route('product.index')}}" class='sidebar-link'>
                <i data-feather="package" width="20">></i>
                <span>Barang</span>
            </a>
        </li>


            <li class="sidebar-item">
                <a href="{{route('supplier.index')}}" class='sidebar-link'>
                    <i data-feather="truck" width="20">></i>
                    <span>Supplier</span>
                </a>
            </li>
             <li class='sidebar-title'>Transaksi Pembelian</li>

        <li class="sidebar-item">
            <a href="{{route('transaksi.pembelian.index')}}" class='sidebar-link'>
                <i data-feather="shopping-cart" width="20">></i>
                <span>List Transaksi Pembelian</span>
            </a>
        </li>

        <li class="sidebar-item">
            <a href="{{route('transaksi.pembelian.add')}}" class='sidebar-link'>
                <i data-feather="shopping-bag" width="20">></i>
                <span>Aplikasi Transaksi Pembelian </span>
            </a>
        </li>

        <li class='sidebar-title'>Transaksi Penjualan</li>

        <li class="sidebar-item">
            <a href="{{route('trans_jual.index')}}" class='sidebar-link'>
                <i data-feather="shopping-cart" width="20">></i>
                <span>List Transaksi Penjualan</span>
            </a>
        </li>

        <li class="sidebar-item">
            <a href="{{route('app.trans')}}" class='sidebar-link'>
                <i data-feather="shopping-bag" width="20">></i>
                <span>Aplikasi Transaksi Penjualan </span>
            </a>
        </li>



{{--        @if(session('roleUserLogin') === 'pemilik_toko' && 'admin')--}}
{{--            <li class='sidebar-title'>Laporan Periodik</li>--}}

{{--            <li class="sidebar-item">--}}
{{--                <a href="#" class='sidebar-link'>--}}
{{--                    <i data-feather="book" width="20">></i>--}}
{{--                    <span>Laporan</span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        @endif--}}

    </ul>
</div>
