<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DetailTransaksiPembelian extends Model
{
    public static function getSupplier($id)
    {
        $query = 'select nama_supplier, nama_perusahaan, email '
            . 'from suppliers join transaksi_pembelian tp on suppliers.id = tp.id_supplier where id = ?';
        return DB::select($query, [$id]);
    }

    public static function getItemTransaksiByNoTransaksi($id)
    {
        return DB::table('detail_transaksi_pembelian')
            ->where('itrx_id_transaksi', '=', $id)
            ->get();
    }
}
