<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Member extends Model
{

    public static function kode()
    {
        $kode = DB::table('members')->max('id_member');
        $addNol = '';
        $kode = str_replace("MEM", "", $kode);
        $kode = (int) $kode + 1;
        $incrementKode = $kode;

        if (strlen($kode) == 1) {
            $addNol = "000";
        } elseif (strlen($kode) == 2) {
            $addNol = "00";
        } elseif (strlen($kode == 3)) {
            $addNol = "0";
        }

        $kodeBaru = "MEM".$addNol.$incrementKode;
        return $kodeBaru;
    }
}
