<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    public static function generateId()
    {
        $kode = DB::table('products')->max('id_barang');
        $addNol = '';
        $kode = str_replace("PRD", "", $kode);
        $kode = (int)$kode + 1;
        $incrementKode = $kode;

        if (strlen($kode) == 1) {
            $addNol = "000";
        } elseif (strlen($kode) == 2) {
            $addNol = "00";
        } elseif (strlen($kode == 3)) {
            $addNol = "0";
        }

        $kodeBaru = "PRD" . $addNol . $incrementKode;
        return $kodeBaru;
    }
}
