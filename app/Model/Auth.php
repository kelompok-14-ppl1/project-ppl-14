<?php

namespace App\Model;

use Illuminate\Support\Facades\DB;

class Auth
{
    public static function loginVerify($username, $password){

//        //cara pertama
//        $sql = "select*from users where username='$username'";
//        $user = DB::select($sql);

        //cara kedua
        $sql = "select * from users where username=?";
        $user = collect(DB::select($sql, [$username]))->first();

        if ($user != null){
          if (password_verify($password, $user->password)){
              return $user;
              //dd("password bener");
          }
          //dd("password salah");
        }
        return false;
    }

    public static function createToken($idUser){
        $token = md5(date('Y-m-d H:i:s') . $idUser);
        DB::table('users')
            ->where('id','=',$idUser)
            ->update(['token' => $token]);

        return $token;
    }

}
