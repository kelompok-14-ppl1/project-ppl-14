<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TransaksiPembelian extends Model
{
    public static function getAll()
    {
        return DB::table('transaksi_pembelian')->get();
    }

    public static function getNewNomor()
    {
        $query = 'select ifnull(max(nomor),0) as nomor '
            . 'from transaksi_pembelian '
            . 'where year(now()) = year(tanggal_pembelian)  '
            . 'AND month(now()) = month(tanggal_pembelian)';
        return (int)collect(DB::select($query))->first()->nomor + 1;
    }

    public static function getNomerTransaksiTerbaru($newNomor)
    {
        return 'TRX-'
            . date('Y') . '-'
            . date('M') . '-'
            . str_pad($newNomor, 4, '0', STR_PAD_LEFT);
    }

    public static function getIdKaryByIdUser($id)
    {
        return collect(DB::table('employees')
            ->select('id_karyawan')
            ->where('id_user', '=', $id)
            ->first());
    }

    public static function getBySupplierId($id)
    {
        $query =  DB::table('suppliers')
            ->join('transaksi_pembelian', 'transaksi_pembelian.id_supplier', '=', 'suppliers.id')
            ->where('suppliers.id', '=', $id)
            ->get();
        return $query;
    }
//
//    public function getDetailTransaksi($id_transaksi_penjualan)
//    {
//
//        return DB::table('sales_detail_transactions')
//            ->where('id_transaksi_penjualan', '=', $id_transaksi_penjualan)
//            ->get();
//    }
//

    public static function getDetailTransaksi($id_transaksi_pembelian){
        return DB::table('detail_transaksi_pembelian')
            ->where('itrx_id_transaksi', '=', $id_transaksi_pembelian)
            ->get();
    }
    public static function getTransaksiById($id_transaksi_pembelian){
//        select members.nama_member, employees.nama_karyawan, sales_transactions.id_transaksi_penjualan,
//       sales_transactions.tanggal_transaksi_penjualan
//FROM members, employees, sales_transactions
//WHERE members.id_member=sales_transactions.id_member_trans AND
//        employees.id_karyawan=sales_transactions.id_karyawan_penjualan AND
//        sales_transactions.id_transaksi_penjualan='TRX001';

//        return DB::table('sales_transactions')
//            ->where('id_transaksi_penjualan','=', $id_transaksi_penjualan)
//            ->get();

//        return DB::table('transaksi_pembelian')
//            ->select('employees.nama_karyawan','suppliers.nama_supplier','transaksi_pembelian.nomor_transaksi','transaksi_pembelian.tanggal_pembelian')
//            ->join('employees', 'id_karyawan','=','transaksi_pembelian.id_karyawan')
//            ->join('suppliers','id','=','transaksi_pembelian.id_supplier')
//            ->where('transaksi_pembelian.nomor_transaksi','=',$id_transaksi_pembelian)
//            ->get();

        return DB::table('transaksi_pembelian')
            ->select('suppliers.nama_supplier','transaksi_pembelian.nomor_transaksi','transaksi_pembelian.tanggal_pembelian')
//            ->join('employees', 'id_karyawan','=','transaksi_pembelian.id_karyawan')
            ->join('suppliers','id','=','transaksi_pembelian.id_supplier')
            ->where('transaksi_pembelian.nomor_transaksi','=', $id_transaksi_pembelian)->get();
    }
}
