<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User extends Model
{
    use HasFactory;

    protected $table = 'users';
    protected $primaryKey = 'id';

    protected $fillable =[
        'nama',
        'username',
        'password',
        'role'
    ];

    public static function getByWebToken($token){
        return DB::table('users')
            ->where('token','=', $token)
            ->first();
    }

    public static function kode()
    {
        $kode = DB::table('users')->max('id');
        $addNol = '';
        $kode = str_replace("US", "", $kode);
        $kode = (int) $kode + 1;
        $incrementKode = $kode;

        if (strlen($kode) == 1) {
            $addNol = "000";
        } elseif (strlen($kode) == 2) {
            $addNol = "00";
        } elseif (strlen($kode == 3)) {
            $addNol = "0";
        }

        $kodeBaru = "US".$addNol.$incrementKode;
        return $kodeBaru;
    }

    public static function getAll(){
        return DB::table('users')->get();
    }



}
