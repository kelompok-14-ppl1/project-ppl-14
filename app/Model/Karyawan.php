<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Karyawan extends Model
{

    protected $table = 'employees';

    public static function getAll()
    {
        return DB::table('employees')->get();
    }

    public static function getById()
    {
        return DB::table('employees')
            ->select('id_karyawan')
            ->where('id_user', '=', session('iduserlogin'))
            ->first();
    }
    public static function kode()
    {
        $kode = DB::table('employees')->max('id_karyawan');
        $addNol = '';
        $kode = str_replace("KARY", "", $kode);
        $kode = (int) $kode + 1;
        $incrementKode = $kode;

        if (strlen($kode) == 1) {
            $addNol = "000";
        } elseif (strlen($kode) == 2) {
            $addNol = "00";
        } elseif (strlen($kode == 3)) {
            $addNol = "0";
        }

        $kodeBaru = "KARY".$addNol.$incrementKode;
        return $kodeBaru;
    }

}
