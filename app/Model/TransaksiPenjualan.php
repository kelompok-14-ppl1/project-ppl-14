<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TransaksiPenjualan extends Model
{
    public static function getAll()
    {
        return DB::table('sales_transactions')->get();
    }

    public static function getDetailTransaksi($id_transaksi_penjualan)
    {
        return DB::table('sales_detail_transactions')
            ->where('id_transaksi_penjualan', '=', $id_transaksi_penjualan)
            ->get();
    }

    public static function getNewNomor()
    {
        $query = 'select ifnull(max(nomor),0) as nomor '
            . 'from sales_transactions '
            . 'where year(now()) = year(tanggal_transaksi_penjualan)  '
            . 'AND month(now()) = month(tanggal_transaksi_penjualan)';
        return (int)collect(DB::select($query))->first()->nomor + 1;
    }

    public static function getNomerTransaksiTerbaru($newNomor)
    {
        return 'TRX-'
            . date('Y') . '-'
            . date('M') . '-'
            . str_pad($newNomor, 4, '0', STR_PAD_LEFT);
    }

    public static function minusStockBarang($id, int $jumlah)
    {
        $query = "update products set stock_barang = stock_barang - $jumlah "
            . "where id_barang = '$id'";
        return DB::select($query);
    }

    public static function getTransaksiById($id_transaksi_penjualan)
    {
//        select members.nama_member, employees.nama_karyawan, sales_transactions.id_transaksi_penjualan,
//       sales_transactions.tanggal_transaksi_penjualan
//FROM members, employees, sales_transactions
//WHERE members.id_member=sales_transactions.id_member_trans AND
//        employees.id_karyawan=sales_transactions.id_karyawan_penjualan AND
//        sales_transactions.id_transaksi_penjualan='TRX001';

//        return DB::table('sales_transactions')
//            ->where('id_transaksi_penjualan','=', $id_transaksi_penjualan)
//            ->get();

        return DB::table('sales_transactions')
            ->select('members.nama_member', 'employees.nama_karyawan', 'sales_transactions.id_transaksi_penjualan', 'sales_transactions.tanggal_transaksi_penjualan','sales_transactions.diskon')
            ->join('members', 'id_member', '=', 'sales_transactions.id_member_trans')
            ->join('employees', 'id_karyawan', '=', 'sales_transactions.id_karyawan_penjualan')
            ->where('sales_transactions.id_transaksi_penjualan', '=', $id_transaksi_penjualan)
            ->get();
    }
}
