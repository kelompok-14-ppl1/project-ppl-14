<?php

namespace App\Http\Middleware;

use Closure;

class SuperadmindanAdminMiddleware
{
    public function handle($request,  Closure  $next){
        if (session('roleUserLogin')  !== 'pemilik_toko' && 'admin'){
            return response("Anda Hanya Bisa Mengakses Role Anda!");
        }
        return $next($request);
//        if (session('roleUserLogin') === 'superadmin' && 'admin'){
//
//
//        }
//        $request->token = session('token');
//        return $next($request);
    }
}


