<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\BaseController;
use App\Model\Karyawan;
use App\Model\User;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class KaryawanController extends BaseController
{
    public function index()
    {
        $data = [
            'title' => "Data Karyawan",
            'all_karyawan' => DB::table('employees')->get(),
            'kode_karyawan' => Karyawan::kode(),
            'kode_user' => User::kode(),
        ];
        return view('content/karyawan', $data);
    }

    public function createKaryawan(Request $request)
    {
        $data = [
            'titleTambahKaryawan' => 'Tambah Karyawan'
        ];

        $user = new User();
        $user->id = $request->post('id');
        $user->nama = $request->post('nama_karyawan');
        $user->username = $request->post('username');
        $user->password = Hash::make($request->post('password'));
        $user->role = $request->post('role');

        if ($user->save()) {
            /*Jika tidak ada error yang terjadi*/
            DB::table('employees')->insert([
                'id_karyawan' => $request->post('id_karyawan'),
                'nama_karyawan' => $request->post('nama_karyawan'),
                'tmpt_lahir' => $request->post('tmpt_lahir'),
                'tggl_lahir' => $request->post('tggl_lahir'),
                'jenis_kel' => $request->post('jenis_kel'),
                'alamat_kary' => $request->post('alamat_kary'),
                'status_kary' => $request->post('status_kary'),
                'id_user' => $request->post('id'),
            ]);
        } else {
            return redirect(route('karyawan.index', $data))->with('gagal', 'Data User Gagal Ditambah');
        }
        return redirect(route('karyawan.index', $data));
    }

    public function editKaryawan($id)
    {
        DB::table('employees')
            ->where('id_karyawan', '=', $id)
            ->update([
                'id_karyawan' => request('id_karyawan'),
                'nama_karyawan' => request('nama_karyawan'),
                'tmpt_lahir' => request('tmpt_lahir'),
                'tggl_lahir' => request('tggl_lahir'),
                'jenis_kel' => request('jenis_kel'),
                'alamat_kary' => request('alamat_kary'),
                'status_kary' => request('status_kary'),
            ]);

        return redirect(route('karyawan.index'));
    }

    public function statusKaryawan($id)
    {
        $karyawan = DB::table('employees')
            ->select('status_kary')
            ->where('id_karyawan', '=', $id)
            ->first();

        if ($karyawan->status_kary == '1') {
            $status_kary = '0';
        } else {
            $status_kary = '1';
        }
        //update karyawan status
        $status = array('status_kary' => $status_kary);
        DB::table('employees')->where('id_karyawan', $id)->update($status);

        return redirect(route('karyawan.index'));
    }

    public function cetakEmployeesPdf()
    {
        $fpdf = new FPDF('L', 'mm', 'A4');
        $tanggalDicetak = date('d M Y');
        $dicetakOleh = session('namaUserLogin');
        $dataSupplier = DB::table('employees')->get();


        $fpdf->AddPage();

        $fpdf->SetFont('Courier', 'B', '20');
        $fpdf->Cell('0', '10', 'SITOKER', 0, 1, 'C');
        $fpdf->SetFont('Courier', 'B', '15');
        $fpdf->Cell('0', '10', 'Rekap Data Karyawan', 0, 1, 'C');
        $fpdf->SetFont('Courier', 'B', '20');
        $fpdf->Cell(10, 5, '----------------------------------------------------------------', 0, 1);

        $fpdf->Cell(10, 7, '', 0, 1);
        $fpdf->SetFont('Courier', '', 12);
        $fpdf->Cell(15, 10, "Dicetak Tanggal        : " . $tanggalDicetak, 0, 1);
        $fpdf->Cell(15, 10, "Dicetak Oleh           : " . $dicetakOleh);

        $fpdf->Cell(10, 15, '', 0, 1);

        $fpdf->SetFont('Courier', '', 10);
        $fpdf->Cell(30, 10, 'Kode ', 1, 0, 'C');
        $fpdf->Cell(60, 10, 'Nama ', 1, 0, 'C');
        $fpdf->Cell(50, 10, 'Tempat Lahir', 1, 0, 'C');
        $fpdf->Cell(60, 10, 'Tanggal Lahir', 1, 0, 'C');
        $fpdf->Cell(20, 10, 'JK', 1, 0, 'C');
        $fpdf->Cell(53, 10, 'Alamat', 1, 1, 'C');


        foreach ($dataSupplier as $data) {
            $fpdf->Cell(30, 10, $data->id_karyawan, 1, 0, 'C');
            $fpdf->Cell(60, 10, $data->nama_karyawan, 1, 0, 'L');
            $fpdf->Cell(50, 10, $data->tmpt_lahir, 1, 0, 'L');
            $fpdf->Cell(60, 10, $data->tggl_lahir, 1, 0, 'L');
            $fpdf->Cell(20, 10, $data->jenis_kel == 'L' ? 'Laki-Laki' : 'Perempuan', 1, 0, 'C');
            $fpdf->Cell(53, 10, $data->alamat_kary, 1, 1, 'L');
        }


        $fpdf->SetFont('Courier', '', '12');

        $fpdf->Output('D', 'Rekap Data Karyawan.pdf');
        exit;

    }

    public function exportKaryawanExcel()
    {
        $tanggalDicetak = date('d M Y');
        $dicetakOleh = session('namaUserLogin');
        $karyawan = DB::table('employees')->get();


        $filename = "Rekap Data Karyawan.xls";

        $excel = new Spreadsheet();

        $sheet = $excel->getActiveSheet();
        $sheet->setCellValue('A1', 'SITOKER')->mergeCells('A1:E1');
        $sheet->setCellValue('A2', 'Rekap Data Karyawan')->mergeCells('A2:E2');

        $sheet->setCellValue('A4', 'Dicetak Tanggal : ' . $tanggalDicetak)->mergeCells('A4:E4');
        $sheet->setCellValue('A5', 'Dicetak Oleh : ' . $dicetakOleh)->mergeCells('A5:E5');

        $sheet->setCellValue('A7', 'NO');
        $sheet->setCellValue('B7', 'Kode Karyawan');
        $sheet->setCellValue('C7', 'Tempat Lahir');
        $sheet->setCellValue('D7', 'Tanggal Lahir');
        $sheet->setCellValue('E7', 'Tanggal Lahir');
        $sheet->setCellValue('F7', 'Jenis Kelamin');
        $sheet->setCellValue('G7', 'Alamat');
        $sheet->setCellValue('H7', 'Status Karyawan');

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $row = 8;
        $no = 1;
        foreach ($karyawan as $data) {
            $sheet->setCellValue("A$row", $no++);
            $sheet->setCellValue("B$row", $data->id_karyawan);
            $sheet->setCellValue("C$row", $data->nama_karyawan);
            $sheet->setCellValue("D$row", $data->tmpt_lahir);
            $sheet->setCellValue("E$row", $data->tggl_lahir);
            $sheet->setCellValue("F$row", $data->jenis_kel == 'L' ? 'Laki-Laki' : 'Perempuan');
            $sheet->setCellValue("G$row", $data->alamat_kary);
            $sheet->setCellValue("H$row", $data->status_kary == '1' ? 'Aktif' : 'Tidak Aktif');
            $row++;
        }

        $writer = new Xls($excel);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
    }

}
