<?php

namespace App\Http\Controllers\Base;

use App\Http\Controllers\Controller;
use App\Model\User;
use Illuminate\Support\Facades\Redirect;

abstract class BaseController extends Controller
{

    private $User;

    public function __construct()
    {
        $token = session('token');
        $user = User::getByWebToken($token);
        if ($user === null){
            Redirect::to(route('login.index'))->send();
            exit();
        }
        $this->User = $user;

    }
    protected function getUserID(){
        return $this->User->id;
    }
    protected function getUsername(): string{
        return $this->User->username;
    }
    public function getNameUser(): string{
        return $this->User->nama;
    }
}
