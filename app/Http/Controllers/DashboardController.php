<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\BaseController;
use Illuminate\Support\Facades\DB;

class DashboardController extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'Dashboard',
            'jumlahUser' => DB::table('users')->count(),
            'jumlahKaryawan' => DB::table('employees')->count(),
            'jumlahMember' => DB::table('members')->count(),
            'jumlahBarang' => DB::table('products')->count(),
            'jumlahSupplier' => DB::table('suppliers')->count(),
            'jumlahTrxPembelian' => DB::table('transaksi_pembelian')->count(),
            'jumlahTrxPenjualan' => DB::table('sales_transactions')->count()
        ];
        return view('content/dashboard', $data);
    }

}
