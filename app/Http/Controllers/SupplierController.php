<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\BaseController;
use App\Model\Supplier;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class SupplierController extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'Supplier',
            'list_supplier' => Supplier::all()
        ];
        return view('content/supplier', $data);
    }

    public function addSupplier()
    {
        $supplier = [
            'nama_supplier' => request('nama_supplier'),
            'nama_perusahaan' => request('nama_perusahaan'),
            'alamat_supplier' => request('alamat_supplier'),
            'kode_pos' => request('kode_pos'),
            'email' => request('email'),
        ];

        //Insert Supplier
        DB::table('suppliers')->insert($supplier);
        return redirect(route('supplier.index'));
    }

    public function cetakSupplierPdf()
    {
        $fpdf = new FPDF('L', 'mm', 'A4');
        $tanggalDicetak = date('d M Y');
        $dicetakOleh = session('namaUserLogin');
        $dataSupplier = DB::table('suppliers')->get();


        $fpdf->AddPage();

        $fpdf->SetFont('Courier', 'B', '20');
        $fpdf->Cell('0', '10', 'SITOKER', 0, 1, 'C');
        $fpdf->SetFont('Courier', 'B', '15');
        $fpdf->Cell('0', '10', 'Rekap Data Supplier', 0, 1, 'C');
        $fpdf->SetFont('Courier', 'B', '20');
        $fpdf->Cell(10, 5, '----------------------------------------------------------------', 0, 1);

        $fpdf->Cell(10, 7, '', 0, 1);
        $fpdf->SetFont('Courier', '', 12);
        $fpdf->Cell(15, 10, "Dicetak Tanggal        : " . $tanggalDicetak, 0, 1);
        $fpdf->Cell(15, 10, "Dicetak Oleh           : " . $dicetakOleh);

        $fpdf->Cell(10, 15, '', 0, 1);

        $fpdf->SetFont('Courier', '', 10);
        $fpdf->Cell(30, 10, 'Kode Supplier', 1, 0, 'C');
        $fpdf->Cell(60, 10, 'Nama Supplier', 1, 0, 'C');
        $fpdf->Cell(60, 10, 'Nama perusahaan', 1, 0, 'C');
        $fpdf->Cell(60, 10, 'Alamat Perusahaan', 1, 0, 'C');
        $fpdf->Cell(20, 10, 'Kode Pos', 1, 0, 'C');
        $fpdf->Cell(43, 10, 'Email', 1, 1, 'C');


        foreach ($dataSupplier as $data) {
            $fpdf->Cell(30, 10, $data->id, 1, 0, 'C');
            $fpdf->Cell(60, 10, $data->nama_supplier, 1, 0, 'L');
            $fpdf->Cell(60, 10, $data->nama_perusahaan, 1, 0, 'L');
            $fpdf->Cell(60, 10, $data->alamat_supplier, 1, 0, 'L');
            $fpdf->Cell(20, 10, $data->kode_pos, 1, 0, 'C');
            $fpdf->Cell(43, 10, $data->email, 1, 1, 'L');
        }


        $fpdf->SetFont('Courier', '', '12');

        $fpdf->Output('D', 'Rekap Data Supplier.pdf');
        exit;

    }

    public function exportSupplierExcel()
    {
        $tanggalDicetak = date('d M Y');
        $dicetakOleh = session('namaUserLogin');
        $supplier = DB::table('suppliers')->get();


        $filename = "Rekap Data Supplier.xls";

        $excel = new Spreadsheet();

        $sheet = $excel->getActiveSheet();
        $sheet->setCellValue('A1', 'SITOKER')->mergeCells('A1:E1');
        $sheet->setCellValue('A2', 'Rekap Data Supplier')->mergeCells('A2:E2');

        $sheet->setCellValue('A4', 'Dicetak Tanggal : ' . $tanggalDicetak)->mergeCells('A4:E4');
        $sheet->setCellValue('A5', 'Dicetak Oleh : ' . $dicetakOleh)->mergeCells('A5:E5');

        $sheet->setCellValue('A7', 'NO');
        $sheet->setCellValue('B7', 'Kode Supplier');
        $sheet->setCellValue('C7', 'Nama Supplier');
        $sheet->setCellValue('D7', 'Nama Perusahaan');
        $sheet->setCellValue('E7', 'Alamat');
        $sheet->setCellValue('F7', 'Kode Pos');
        $sheet->setCellValue('G7', 'Email');

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);

        $row = 8;
        $no = 1;
        foreach ($supplier as $data) {
            $sheet->setCellValue("A$row", $no++);
            $sheet->setCellValue("B$row", $data->id);
            $sheet->setCellValue("C$row", $data->nama_supplier);
            $sheet->setCellValue("D$row", $data->nama_perusahaan);
            $sheet->setCellValue("E$row", $data->alamat_supplier);
            $sheet->setCellValue("F$row", $data->kode_pos);
            $sheet->setCellValue("G$row", $data->email);
            $row++;
        }

        $writer = new Xls($excel);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
    }

}
