<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\BaseController;
use App\Model\Member;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;


class MemberController extends BaseController
{
    public function index()
    {
        $data = [
            'title' => "Data Member",
            'all_member' => DB::table('members')->get(),
            'kode_member' => Member::kode(),
        ];

        return view('content/member', $data);
    }

    public function createMember(Request $request)
    {
        $data = [
            'titleTambahMember' => 'Tambah Member'
        ];
//        $this->validate($request, [
//            'nama' => 'required',
//            'tempat_lahir' => 'required',
//            'tanggal_lahir' => 'required',
//            'jenis_kelamin' => 'required',
//            'alamat_member' => 'required',
//            'contact_member' => 'required|numeric',
//        ]);
        $validator = Validator::make($data, [
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'alamat_member' => 'required',
            'contact_member' => 'required|numeric',
        ]);
        DB::table('members')->insert([
            'id_member' => $request->post('id_member'),
            'nama_member' => $request->post('nama_member'),
            'tempat_lahir' => $request->post('tempat_lahir'),
            'tanggal_lahir' => $request->post('tanggal_lahir'),
            'jenis_kelamin' => $request->post('jenis_kelamin'),
            'alamat_member' => $request->post('alamat_member'),
            'contact_member' => $request->post('contact_member'),
            'status_member' => $request->post('status_member'),
        ]);
        return redirect(route('member.index', $data));
    }


    public function editMember($id)
    {
        DB::table('members')
            ->where('id_member', '=', $id)
            ->update([
                'nama_member' => request('nama_member'),
                'tempat_lahir' => request('tempat_lahir'),
                'tanggal_lahir' => request('tanggal_lahir'),
                'jenis_kelamin' => request('jenis_kelamin'),
                'alamat_member' => request('alamat_member'),
                'contact_member' => request('contact_member'),
                'status_member' => request('status_member')
            ]);

        return redirect(route('member.index'));
    }

    public function statusMember($id)
    {
        $member = DB::table('members')
            ->select('status_member')
            ->where('id_member', '=', $id)
            ->first();
        if ($member->status_member == '1') {
            $status_member = '0';
        } else {
            $status_member = '1';
        }
        //update member status
        $status = array('status_member' => $status_member);
        DB::table('members')->where('id_member', $id)->update($status);

        return redirect(route('member.index'));
    }

    public function cetakMembersPdf()
    {
        $fpdf = new FPDF('L', 'mm', 'A4');
        $tanggalDicetak = date('d M Y');
        $dicetakOleh = session('namaUserLogin');
        $dataSupplier = DB::table('members')->get();


        $fpdf->AddPage();

        $fpdf->SetFont('Courier', 'B', '20');
        $fpdf->Cell('0', '10', 'SITOKER', 0, 1, 'C');
        $fpdf->SetFont('Courier', 'B', '15');
        $fpdf->Cell('0', '10', 'Rekap Data Members', 0, 1, 'C');
        $fpdf->SetFont('Courier', 'B', '20');
        $fpdf->Cell(10, 5, '----------------------------------------------------------------', 0, 1);

        $fpdf->Cell(10, 7, '', 0, 1);
        $fpdf->SetFont('Courier', '', 12);
        $fpdf->Cell(15, 10, "Dicetak Tanggal        : " . $tanggalDicetak, 0, 1);
        $fpdf->Cell(15, 10, "Dicetak Oleh           : " . $dicetakOleh);

        $fpdf->Cell(10, 15, '', 0, 1);

        $fpdf->SetFont('Courier', '', 10);
        $fpdf->Cell(25, 10, 'Kode ', 1, 0, 'C');
        $fpdf->Cell(60, 10, 'Nama ', 1, 0, 'C');
        $fpdf->Cell(50, 10, 'Tempat Lahir', 1, 0, 'C');
        $fpdf->Cell(30, 10, 'Tanggal Lahir', 1, 0, 'C');
        $fpdf->Cell(20, 10, 'JK', 1, 0, 'C');
        $fpdf->Cell(50, 10, 'Alamat', 1, 0, 'C');
        $fpdf->Cell(40, 10, 'No HP', 1, 1, 'C');


        foreach ($dataSupplier as $data) {
            $fpdf->Cell(25, 10, $data->id_member, 1, 0, 'C');
            $fpdf->Cell(60, 10, $data->nama_member, 1, 0, 'L');
            $fpdf->Cell(50, 10, $data->tempat_lahir, 1, 0, 'L');
            $fpdf->Cell(30, 10, $data->tanggal_lahir, 1, 0, 'L');
            $fpdf->Cell(20, 10, ($data->jenis_kelamin === 'P' ? 'Perempuan' : 'Laki-Laki'), 1, 0, 'C');
            $fpdf->Cell(50, 10, $data->alamat_member, 1, 0, 'L');
            $fpdf->Cell(40, 10, $data->contact_member, 1, 1, 'L');
        }


        $fpdf->SetFont('Courier', '', '12');

        $fpdf->Output('D', 'RekapDataSupplier.pdf');
        exit;

    }

    public
    function cetakKartuMember($id_member)
    {
        $dataMember = DB::table('members')
            ->get()
            ->where('id_member', '=', $id_member);

        $fpdf = new FPDF('L', 'mm', 'A5');
        $tanggalDicetak = date('d M Y');


        $fpdf->AddPage();

        $fpdf->SetFont('Courier', 'B', '20');
        $fpdf->Cell('0', '10', 'SITOKER', 0, 1, 'C');
        $fpdf->SetFont('Courier', 'B', '18');
        $fpdf->Cell('0', '10', 'Kartu Member', 0, 1, 'C');
        $fpdf->SetFont('Courier', 'B', '20');
        $fpdf->Cell(10, 5, '--------------------------------------------', 0, 1);

        $fpdf->Cell(10, 7, '', 0, 1);
        $fpdf->SetFont('Courier', '', 12);

        foreach ($dataMember as $data) {
            $fpdf->Cell(25, 10, "Kode Member        : " . $data->id_member, 0, 1);
            $fpdf->Cell(60, 10, "Nama               : " . $data->nama_member, 0, 1);
            $fpdf->Cell(50, 10, "Tempat Lahir       : " . $data->tempat_lahir, 0, 1);
            $fpdf->Cell(30, 10, "Tanggal Lahir      : " . $data->tanggal_lahir, 0, 1);
            $fpdf->Cell(20, 10, "Jenis Kelamin      : " . ($data->jenis_kelamin === 'P' ? 'Perempuan' : 'Laki-Laki'), 0, 1);
            $fpdf->Cell(50, 10, "Alamat             : " . $data->alamat_member, 0, 1);
            $fpdf->Cell(40, 10, "No HP              : " . $data->contact_member, 0, 1);
        }


        $fpdf->SetFont('Courier', 'I', 8);
        $fpdf->Cell(15, 15, "Dicetak Tanggal : " . $tanggalDicetak, 0, 1, 'L');

        $fpdf->Output('D', 'KartuMember.pdf', 'D');

        exit;
    }

    public function exportMembersExcel(){
        $tanggalDicetak = date('d M Y');
        $dicetakOleh = session('namaUserLogin');
        $supplier = DB::table('members')->get();


        $filename = "Rekap Data Member.xls";

        $excel = new Spreadsheet();

        $sheet = $excel->getActiveSheet();
        $sheet->setCellValue('A1', 'SITOKER')->mergeCells('A1:E1');
        $sheet->setCellValue('A2', 'Rekap Data Member')->mergeCells('A2:E2');

        $sheet->setCellValue('A4', 'Dicetak Tanggal : ' . $tanggalDicetak)->mergeCells('A4:E4');
        $sheet->setCellValue('A5', 'Dicetak Oleh : ' . $dicetakOleh)->mergeCells('A5:E5');

        $sheet->setCellValue('A7', 'NO');
        $sheet->setCellValue('B7', 'Kode Member');
        $sheet->setCellValue('C7', 'Nama Member');
        $sheet->setCellValue('D7', 'Tempat Lahir');
        $sheet->setCellValue('E7', 'Tanggal Lahir');
        $sheet->setCellValue('F7', 'Jenis Kelamin ');
        $sheet->setCellValue('G7', 'Alamat');
        $sheet->setCellValue('H7', 'No HP');
        $sheet->setCellValue('I7', 'Status Member');

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);

        $row = 8;
        $no = 1;
        foreach ($supplier as $data) {
            $sheet->setCellValue("A$row", $no++);
            $sheet->setCellValue("B$row", $data->id_member);
            $sheet->setCellValue("C$row", $data->nama_member);
            $sheet->setCellValue("D$row", $data->tempat_lahir);
            $sheet->setCellValue("E$row", $data->tanggal_lahir);
            $sheet->setCellValue("F$row", $data->jenis_kelamin == 'L' ? 'Laki-Laki' : 'Perempuan');
            $sheet->setCellValue("G$row", $data->alamat_member);
            $sheet->setCellValue("H$row", $data->contact_member);
            $sheet->setCellValue("I$row", $data->status_member);
            $row++;
        }

        $writer = new Xls($excel);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
    }
}
