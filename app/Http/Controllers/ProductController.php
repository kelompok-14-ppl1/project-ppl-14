<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\BaseController;
use App\Model\Product;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class ProductController extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'Barang',
            'list_barang' => Product::all(),
            'kode_barang' => Product::generateId(),
        ];
        return view('content/barang', $data);
    }

    public function addProduct()
    {
        $product = [
            'id_barang' => request('id_barang'),
            'nama_barang' => request('nama_barang'),
            'harga_barang' => request('harga'),
            'stock_barang' => request('stock_barang'),
        ];
        //Insert data ke dalam database
        DB::table('products')->insert($product);
        return redirect(route('product.index'));
    }

    public function updateProduct($id)
    {
        DB::table('products')
            ->where('id_barang', '=', $id)
            ->update([
                'nama_barang' => request('nama_barang'),
                'harga_barang' => request('harga'),
                'stock_barang' => request('stock_barang'),
            ]);
        return redirect(route('product.index'))->with('Sukses', 'Product Berhasil ditambahkan');
    }

    public function deleteProduct($id)
    {
        Product::find($id)->delete();
        return back();
    }

    public function cetakProductsPdf()
    {
        $fpdf = new FPDF('P','mm','A4');
        $tanggalDicetak = date('d M Y');
        $dicetakOleh = session('namaUserLogin');
        $dataProducts = DB::table('products')->get();


        $fpdf->AddPage();

        $fpdf->SetFont('Courier','B','20');
        $fpdf->Cell('0','10','SITOKER', 0, 1,'C');
        $fpdf->SetFont('Courier','B','15');
        $fpdf->Cell('0','10','Rekap Data Barang', 0, 1,'C');
        $fpdf->SetFont('Courier','B','20');
        $fpdf->Cell(10, 5, '-------------------------------------------', 0, 1);

        $fpdf->Cell(10, 7, '', 0, 1);
        $fpdf->SetFont('Courier', '', 12);
        $fpdf->Cell(15, 10, "Dicetak Tanggal        : ". $tanggalDicetak , 0, 1);
        $fpdf->Cell(15, 10, "Dicetak Oleh           : ". $dicetakOleh);

        $fpdf->Cell(10, 15, '', 0, 1);

        $fpdf->SetFont('Courier', '', 10);
        $fpdf->Cell(30,10,'Kode ', 1, 0, 'C');
        $fpdf->Cell(70,10, 'Nama Barang ', 1, 0, 'C');
        $fpdf->Cell(50, 10,'Harga',1,0,'C');
        $fpdf->Cell(40, 10, 'Stok', 1, 1, 'C');



        foreach ($dataProducts as $data){
            $fpdf->Cell(30, 10, $data->id_barang, 1, 0, 'C');
            $fpdf->Cell(70, 10, $data->nama_barang, 1, 0,'L');
            $fpdf->Cell(50, 10, "Rp. ". number_format($data->harga_barang,'0',',','.'), 1, 0,'C');
            $fpdf->Cell(40, 10, $data->stock_barang, 1, 1,'C');
        }



        $fpdf->SetFont('Courier','','12');

        $fpdf->Output('D','RekapDataBarang.pdf');
        exit;

    }
    public function exportProductExcel()
    {
        $tanggalDicetak = date('d M Y');
        $dicetakOleh = session('namaUserLogin');
        $barang = DB::table('products')->get();

        $filename = "Rekap Data Barang.xls";

        $excel = new Spreadsheet();

        $sheet = $excel->getActiveSheet();
        $sheet->setCellValue('A1','SITOKER')->mergeCells('A1:E1');
        $sheet->setCellValue('A2','Rekap Data Barang')->mergeCells('A2:E2');

        $sheet->setCellValue('A4','Dicetak Tanggal : '.$tanggalDicetak)->mergeCells('A4:E4');
        $sheet->setCellValue('A5','Dicetak Oleh : '.$dicetakOleh)->mergeCells('A5:E5');

        $sheet->setCellValue('A7','NO');
        $sheet->setCellValue('B7','Kode Barang');
        $sheet->setCellValue('C7','Nama Barang');
        $sheet->setCellValue('D7','Harga');
        $sheet->setCellValue('E7','Stock');

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);

        $row = 8;
        $no = 1;
        foreach ($barang as $data){
            $sheet->setCellValue("A$row",$no++);
            $sheet->setCellValue("B$row", $data->id_barang);
            $sheet->setCellValue("C$row",$data->nama_barang);
            $sheet->setCellValue("D$row", "Rp." . number_format($data->harga_barang,'0','.','.'));
            $sheet->setCellValue("E$row",$data->stock_barang);
            $row++;
        }

        $writer = new Xls($excel);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' .$filename. '"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
    }

}
