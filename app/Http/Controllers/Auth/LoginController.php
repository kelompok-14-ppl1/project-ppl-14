<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function index()
    {
        return view('layout/login');
    }

    //untuk memverifikasi username dan password yang diinputkan ketika login
    public function verifyLogin()
    {
        //dd(request()->all());
        $username = request('username');
        $password = request('password');
        $user = Auth::loginVerify($username, $password);
        if ($user === false) {
            Session::flash('error', 'Username dan Password Tidak Sesuai!');
            return redirect(route('login.index'));

        }

        //kondisi ketika user tidak null
        $token = Auth::createToken($user->id);
        session(
            [
                'token' => $token,
                'namaUserLogin' => $user->nama,
                'iduserlogin' => $user->id,
                'usernameUserLogin' => $user->username,
                'roleUserLogin' => $user->role,
                'jamLogin' => date('H:i:s'),
                'password' => $user->password,
            ]
        );

        return redirect(route('dashboard.index'))->with('success', 'Login Berhasil!');
    }

    public function logout()
    {
        Session::flush();
        return redirect(route('login.index'))->with('success', 'Kamu Sudah Logout');
    }


}
