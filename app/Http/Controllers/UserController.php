<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\BaseController;
use App\Model\User;
use Codedge\Fpdf\Fpdf\Fpdf;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class UserController extends BaseController
{
    public function index()
    {
        $data = [
            'title' => "Data User",
            'all_user' => DB::table('users')->get(),
            'kode_user' => User::kode(),
        ];
        return view('content/user', $data);
    }


//    public function proses_createUser(Request $request)
//    {
//        $data = [
//            'titleTambahUser' => 'Tambah User'
//        ];
//
//        $request->validate([
//            'nama' => 'required',
//            'username' => 'required',
//            'password' => 'required',
//            'role' => 'required',
//        ], [
//            'nama.required' => 'Nama Tidak Boleh Kosong!',
//            'username.required' => 'Username Tidak Boleh Kosong!',
//            'password.required' => 'Password Tidak Boleh Kosong!',
//            'role.required' => 'Role Tidak Boleh Kosong!',
//        ]);
//
//        $user = new User();
//        $user->id = $request->post('id');
//        $user->nama = $request->post('nama');
//        $user->username = $request->post('username');
//        $user->password = Hash::make($request->post('password'));
//        $user->role = $request->post('role');
//
//        if ($user->save()) {
//            return redirect(route('user.index', $data))->with('sukses', 'Data User Berhasil Ditambah');
//        } else {
//            return redirect(route('user.index', $data))->with('gagal', 'Data User Gagal Ditambah');
//        }
// }

    public function editDataUser($id)
    {
        DB::table('users')
            ->where('id', '=', $id)
            ->update([
                'id' => request('id'),
                'nama' => request('nama'),
                'username' => request('username'),
                'password' => request('password'),
                'role' => request('role')
            ]);

        return redirect(route('user.index'));
    }


    public function ubahPassword(Request $request)
    {
        $passwordLama = $request->input('password_lama');
        $passwordUserLogin = session('password');
        $idUserLogin = session('iduserlogin');
        $check = Hash::check($passwordLama, $passwordUserLogin);

        if ($idUserLogin !== null) {
            if ($check === false) {
                return redirect(route('dashboard.index'));
            }

            DB::table('users')
                ->where('id', '=', $idUserLogin)
                ->update([
                    'password' => Hash::make($request->input('password'))
                ]);

            // $updatePassword = Hash::make($request->input('password'));
            return redirect(route('login.index'));
            // $this->logout();
        }


//        $data = User::find(\auth()->user());
//        $check = Hash::check($password, User::find(auth()->loginUsingId('id')));
//
//        if ($data !== null){
//            if ($check == false){
//                return redirect()->back();
//            }
//
//            $data->password = Hash::make($request->input('password'));
//            $data->save();
//            $this->logout();
//        }
        return redirect(route('dashboard.index'));
    }

    public function cetakUserPdf()
    {
        $fpdf = new FPDF('P','mm','A4');
        $tanggalDicetak = date('d M Y');
        $dicetakOleh = session('namaUserLogin');
        $datauser = DB::table('users')->get();


        $fpdf->AddPage();

        $fpdf->SetFont('Courier','B','20');
        $fpdf->Cell('0','10','SITOKER', 0, 1,'C');
        $fpdf->SetFont('Courier','B','15');
        $fpdf->Cell('0','10','Rekap Data User', 0, 1,'C');
        $fpdf->SetFont('Courier','B','20');
        $fpdf->Cell(10, 5, '---------------------------------------------', 0, 1);

        $fpdf->Cell(10, 7, '', 0, 1);
        $fpdf->SetFont('Courier', '', 12);
        $fpdf->Cell(15, 10, "Dicetak Tanggal        : ". $tanggalDicetak , 0, 1);
        $fpdf->Cell(15, 10, "Dicetak Oleh           : ". $dicetakOleh);

        $fpdf->Cell(10, 15, '', 0, 1);

        $fpdf->SetFont('Courier', '', 10);
        $fpdf->Cell(10, 10, 'No', 1, 0, 'C');
        $fpdf->Cell(25, 10, 'Kode User', 1, 0, 'C');
        $fpdf->Cell(75, 10, 'Nama', 1, 0, 'C');
        $fpdf->Cell(43, 10, 'Username', 1, 0, 'C');
        $fpdf->Cell(43, 10, 'Role', 1, 1, 'C');


        $no =1;
        foreach ($datauser as $data){
            $fpdf->Cell(10, 10, $no, 1, 0,'C');
            $fpdf->Cell(25, 10, $data->id, 1, 0, 'L');
            $fpdf->Cell(75, 10, $data->nama, 1, 0,'L');
            $fpdf->Cell(43, 10, $data->username , 1, 0,'L');
            $fpdf->Cell(43, 10, $data->role, 1, 1,'L');
            $no++;
        }



        $fpdf->SetFont('Courier','','12');

        $fpdf->Output('D','Rekap Data User.pdf');
        exit;

  }

    public function exportUserExcel()
    {
        $tanggalDicetak = date('d M Y');
        $dicetakOleh = session('namaUserLogin');
        $User = DB::table('users')->get();

        $filename = "Rekap Data User.xls";

        $excel = new Spreadsheet();

        $sheet = $excel->getActiveSheet();
        $sheet->setCellValue('A1','SITOKER')->mergeCells('A1:E1');
        $sheet->setCellValue('A2','Rekap Data User')->mergeCells('A2:E2');

        $sheet->setCellValue('A4','Dicetak Tanggal : '.$tanggalDicetak)->mergeCells('A4:E4');
        $sheet->setCellValue('A5','Dicetak Oleh : '.$dicetakOleh)->mergeCells('A5:E5');

        $sheet->setCellValue('A7','NO');
        $sheet->setCellValue('B7','Kode User');
        $sheet->setCellValue('C7','Nama');
        $sheet->setCellValue('D7','Username');
        $sheet->setCellValue('E7','Role');

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);

        $row = 8;
        $no = 1;
        foreach ($User as $data){
            $sheet->setCellValue("A$row",$no++);
            $sheet->setCellValue("B$row",$data->id);
            $sheet->setCellValue("C$row",$data->nama);
            $sheet->setCellValue("D$row",$data->username);
            $sheet->setCellValue("E$row",$data->role);
            $row++;
        }

        $writer = new Xls($excel);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' .$filename. '"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
    }

}
