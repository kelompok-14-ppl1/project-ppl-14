<?php

namespace App\Http\Controllers\Transaksi;

use App\Http\Controllers\Base\BaseController;
use App\Model\Karyawan;
use App\Model\Member;
use App\Model\TransaksiPenjualan;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class TransaksiPenjualanController extends BaseController
{
    public function index()
    {
        $data = [
            'title' => "Daftar Transaksi Penjualan",
            'trans' => TransaksiPenjualan::getAll(),
            'kode_member' => Member::kode(),
        ];
        return view('app/Transaksi_Penjualan/app_trans_jual', $data);
    }

    public function formTransaksiJual()
    {
        $data = [
            'title' => "Transaksi Penjualan",
            'kode_member' => Member::kode(),
        ];
        return view('app/Transaksi_Penjualan/form_tambah_trans_jual', $data);
    }

    public function prosesTransaksi()
    {
        $data = request('sales_item_transaksi');

        $employee = Karyawan::getById();
        $newTransaksi = TransaksiPenjualan::getNewNomor();
        $noTransaksi = TransaksiPenjualan::getNomerTransaksiTerbaru($newTransaksi);
        $sales_transaksi = [
            'id_transaksi_penjualan' => $noTransaksi,
            'tanggal_transaksi_penjualan' => date('Y-m-d H:i:s'),
            'id_member_trans' => request('kodeMember'),
            'id_karyawan_penjualan' => $employee->id_karyawan,
            'diskon' => request('discount'),
            'nomor' => $newTransaksi,
        ];
        DB::table('sales_transactions')->insert($sales_transaksi);
        $sales_item_transaksi = [];
        foreach ($data as $row) {
            $row['id_transaksi_penjualan'] = $noTransaksi;
            $sales_item_transaksi[] = $row;
            //Update stock barang
            $stock = (int)$row['jumlah_barang'];
            TransaksiPenjualan::minusStockBarang($row['id_barang'], $stock);
        }
        DB::table('sales_detail_transactions')->insert($sales_item_transaksi);
        echo '1';
    }

    public function detailTransaksi($id_transaksi_penjualan)
    {
        $data = [
            'title' => 'Invoice Transaksi Penjualan ' . $id_transaksi_penjualan,
            'transaksi' => TransaksiPenjualan::getTransaksiById($id_transaksi_penjualan),
            'detailTransaksi' => TransaksiPenjualan::getDetailTransaksi($id_transaksi_penjualan)
        ];
        return view('app/Transaksi_Penjualan/detail', $data);

    }

    public function cetakInvoiceTransaksi($id_transaksi_penjualan)
    {
        $transaksi = TransaksiPenjualan::getTransaksiById($id_transaksi_penjualan);
        $detailTransaksi = TransaksiPenjualan::getDetailTransaksi($id_transaksi_penjualan);

        $name = "Invoice Transaksi Penjualan $id_transaksi_penjualan.pdf";

        $fpdf = new FPDF('P', 'mm', 'A4');
        $tanggalDicetak = date('d M Y');
        $dicetakOleh = session('namaUserLogin');

        $fpdf->AddPage();

        $fpdf->SetFont('Courier', 'B', '20');
        $fpdf->Cell('0', '10', 'SITOKER', 0, 1, 'C');
        $fpdf->SetFont('Courier', 'B', '15');
        $fpdf->Cell('0', '10', 'Invoice Transaksi ' . $id_transaksi_penjualan, 0, 1, 'C');
        $fpdf->SetFont('Courier', 'B', '20');
        $fpdf->Cell(10, 5, '---------------------------------------------', 0, 1);

        $fpdf->Cell(10, 7, '', 0, 1);
        $fpdf->SetFont('Courier', '', 12);

        foreach ($transaksi as $data) {
            $fpdf->Cell(35, 5, 'Kode Transaksi       : ' . $data->id_transaksi_penjualan, 0, 1, 'L');
            $fpdf->Cell(52, 5, 'Tanggal Transaksi    : ' . $data->tanggal_transaksi_penjualan . 'WIB', 0, 1, 'L');
            $fpdf->Cell(50, 5, 'Nama Member          : ' . $data->nama_member, 0, 1, 'L');
            $fpdf->Cell(50, 5, 'Nama Kasir           : ' . $data->nama_karyawan, 0, 1, 'L');
        }
        $fpdf->Cell(15, 5, 'Dicetak Tanggal      : ' . $tanggalDicetak, 0, 1, 'L');
        $fpdf->Cell(15, 5, 'Dicetak Oleh Kasir   : ' . $dicetakOleh, '', '', 'L');

        $fpdf->Cell(10, 15, '', 0, 1);

        foreach ($detailTransaksi as $data) {
            $fpdf->Cell(35, 5, 'Total Belanja       : ', 0, 1, 'L');
        }

        $fpdf->SetFont('Courier', 'B', 10);
        $fpdf->Cell(35, 10, 'Nama Barang', 1, 0, 'C');
        $fpdf->Cell(52, 10, 'Harga', 1, 0, 'C');
        $fpdf->Cell(50, 10, 'Jumlah', 1, 0, 'C');
        $fpdf->Cell(50, 10, 'Sub Total', 1, 1, 'C');

        $fpdf->SetFont('Courier', '', 10);
        foreach ($detailTransaksi as $data) {
            $fpdf->Cell(35, 10, $data->nama_barang, 1, 0, 'C');
            $fpdf->Cell(52, 10, $data->harga_barang . " WIB", 1, 0, 'L');
            $fpdf->Cell(50, 10, $data->jumlah_barang, 1, 0, 'L');
            $fpdf->Cell(50, 10, $data->total_harga_penjualan, 1, 1, 'L');
        }
        $fpdf->Cell(10, 15, '', 0, 1);


        $fpdf->Output('D', $name);
        exit;
    }

    public function cetakTransaksiPenjualanPdf()
    {
        $dataTrxPenjualan = DB::table('sales_transactions')
            ->join('employees', 'employees.id_karyawan', '=', 'sales_transactions.id_karyawan_penjualan')
            ->join('members', 'members.id_member', '=', 'sales_transactions.id_member_trans')->get();


        $fpdf = new FPDF('P', 'mm', 'A4');
        $tanggalDicetak = date('d M Y');
        $dicetakOleh = session('namaUserLogin');


        $fpdf->AddPage();

        $fpdf->SetFont('Courier', 'B', '20');
        $fpdf->Cell('0', '10', 'SITOKER', 0, 1, 'C');
        $fpdf->SetFont('Courier', 'B', '15');
        $fpdf->Cell('0', '10', 'Rekap Data Transaksi Penjualan', 0, 1, 'C');
        $fpdf->SetFont('Courier', 'B', '20');
        $fpdf->Cell(10, 5, '---------------------------------------------', 0, 1);

        $fpdf->Cell(10, 7, '', 0, 1);
        $fpdf->SetFont('Courier', '', 12);
        $fpdf->Cell(15, 10, "Dicetak Tanggal        : " . $tanggalDicetak, 0, 1);
        $fpdf->Cell(15, 10, "Dicetak Oleh           : " . $dicetakOleh);

        $fpdf->Cell(10, 15, '', 0, 1);

        $fpdf->SetFont('Courier', '', 10);
        $fpdf->Cell(10, 10, 'No', 1, 0, 'C');
        $fpdf->Cell(35, 10, 'Kode Transaksi', 1, 0, 'C');
        $fpdf->Cell(52, 10, 'Tanggal Penjualan', 1, 0, 'C');
        $fpdf->Cell(50, 10, 'Nama Member', 1, 0, 'C');
        $fpdf->Cell(50, 10, 'Nama Karyawan', 1, 1, 'C');


        $no = 1;
        foreach ($dataTrxPenjualan as $data) {
            $fpdf->Cell(10, 10, $no, 1, 0, 'C');
            $fpdf->Cell(35, 10, $data->id_transaksi_penjualan, 1, 0, 'L');
            $fpdf->Cell(52, 10, $data->tanggal_transaksi_penjualan . " WIB", 1, 0, 'L');
            $fpdf->Cell(50, 10, $data->nama_member, 1, 0, 'L');
            $fpdf->Cell(50, 10, $data->nama_karyawan, 1, 1, 'L');
            $no++;
        }


        $fpdf->SetFont('Courier', '', '12');

        $fpdf->Output('D', 'Rekap Data Transaksi Penjualan.pdf');
        exit;

    }

    public function exportTransaksiPenjualanExcel()
    {

        $dataTrxPenjualan = DB::table('sales_transactions')
            ->join('employees', 'employees.id_karyawan', '=', 'sales_transactions.id_karyawan_penjualan')
            ->join('members', 'members.id_member', '=', 'sales_transactions.id_member_trans')->get();

        $tanggalDicetak = date('d M Y');
        $dicetakOleh = session('namaUserLogin');


        $filename = "Rekap Data Transaksi Penjualan.xls";

        $excel = new Spreadsheet();

        $sheet = $excel->getActiveSheet();
        $sheet->setCellValue('A1', 'SITOKER')->mergeCells('A1:E1');
        $sheet->setCellValue('A2', 'Rekap Data Transaksi Penjualan')->mergeCells('A2:E2');

        $sheet->setCellValue('A4', 'Dicetak Tanggal : ' . $tanggalDicetak)->mergeCells('A4:E4');
        $sheet->setCellValue('A5', 'Dicetak Oleh : ' . $dicetakOleh)->mergeCells('A5:E5');

        $sheet->setCellValue('A7', 'NO');
        $sheet->setCellValue('B7', 'Kode Transaksi');
        $sheet->setCellValue('C7', 'Tanggal Penjualan');
        $sheet->setCellValue('D7', 'Nama Member');
        $sheet->setCellValue('E7', 'Nama Karyawan');

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);


        $row = 8;
        $no = 1;
        foreach ($dataTrxPenjualan as $data) {
            $sheet->setCellValue("A$row", $no++);
            $sheet->setCellValue("B$row", $data->id_transaksi_penjualan);
            $sheet->setCellValue("C$row", $data->tanggal_transaksi_penjualan);
            $sheet->setCellValue("D$row", $data->nama_member);
            $sheet->setCellValue("E$row", $data->nama_karyawan);
            $row++;
        }

        $writer = new Xls($excel);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
    }
}
