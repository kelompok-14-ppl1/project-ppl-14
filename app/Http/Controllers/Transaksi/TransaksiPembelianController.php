<?php

namespace App\Http\Controllers\Transaksi;

use App\Http\Controllers\Base\BaseController;
use App\Model\Karyawan;
use App\Model\Supplier;
use App\Model\TransaksiPembelian;
use App\Model\TransaksiPenjualan;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\DB;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class TransaksiPembelianController extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'List Transaksi Pembelian',
            'list_transaksi' => TransaksiPembelian::getAll(),
        ];
        return view('app/Transaksi Pembelian/index', $data);
    }

    public function addTransaksi()
    {
        $data = [
            'title' => 'Transaksi Pembelian',
            'list_supplier' => Supplier::all(),
        ];

        return view('app/Transaksi Pembelian/tambah', $data);
    }

    /**
     * Function proses transaksi
     */
    public function prosesTransaksiPembelian()
    {
        $data = request('item_transaksi');
        $employees = Karyawan::getById();
        $newTransaksi = TransaksiPembelian::getNewNomor();
        $noTransaksi = TransaksiPembelian::getNomerTransaksiTerbaru($newTransaksi);
        $transaksi = [
            'nomor_transaksi' => $noTransaksi,
            'tanggal_pembelian' => date('Y-m-d H:i:s'),
            'id_karyawan' => $employees->id_karyawan,
            'id_supplier' => request('supplier'),
            'nomor' => $newTransaksi,
        ];
        DB::table('transaksi_pembelian')->insert($transaksi);
        $dataDetailTransaksi = [];
        foreach ($data as $row) {
            $row['itrx_id_transaksi'] = $noTransaksi;
            $dataDetailTransaksi[] = $row;
        }
        DB::table('detail_transaksi_pembelian')->insert($dataDetailTransaksi);
        echo '1';
    }

    public function detailTransaksi($id_transaksi_pembelian){
        $data = [
            'title' => 'Invoice Transaksi Pembelian '. $id_transaksi_pembelian,
            'transaksi' => TransaksiPembelian::getTransaksiById($id_transaksi_pembelian),
            'detailTransaksi' => TransaksiPembelian::getDetailTransaksi($id_transaksi_pembelian)
        ];
        return view('app/Transaksi Pembelian/detail', $data);

    }
    public function cetakInvoiceTransaksi($id_transaksi_pembelian){
        $transaksi = TransaksiPembelian::getTransaksiById($id_transaksi_pembelian);
        $detailTransaksi = TransaksiPembelian::getDetailTransaksi($id_transaksi_pembelian);

        $name = "Invoice Transaksi Pembelian $id_transaksi_pembelian.pdf";

        $fpdf = new FPDF('P','mm','A4');
        $tanggalDicetak = date('d M Y');
        $dicetakOleh = session('namaUserLogin');

        $fpdf->AddPage();

        $fpdf->SetFont('Courier','B','20');
        $fpdf->Cell('0','10','SITOKER', 0, 1,'C');
        $fpdf->SetFont('Courier','B','15');
        $fpdf->Cell('0','10','Invoice Transaksi '. $id_transaksi_pembelian, 0, 1,'C');
        $fpdf->SetFont('Courier','B','20');
        $fpdf->Cell(10, 5, '---------------------------------------------', 0, 1);

        $fpdf->Cell(10, 7, '', 0, 1);
        $fpdf->SetFont('Courier', '', 12);

        foreach ($transaksi  as $data){
            $fpdf->Cell(35, 5, 'Kode Transaksi       : ' .$data->nomor_transaksi, 0, 1, 'L');
            $fpdf->Cell(52, 5, 'Tanggal Transaksi    : ' .$data->tanggal_pembelian .'WIB', 0, 1, 'L');
            $fpdf->Cell(50, 5, 'Supplier             : ' .$data->nama_supplier, 0, 1, 'L');
       }
        $fpdf->Cell(15, 5, 'Dicetak Tanggal      : '.$tanggalDicetak , 0, 1,'L');
        $fpdf->Cell(15, 5, 'Dicetak Oleh Kasir   : '.$dicetakOleh ,'','','L');

        $fpdf->Cell(10, 15, '', 0, 1);
        foreach ($detailTransaksi  as $data){
            $fpdf->Cell(35, 5, 'Total Belanja       : Rp.  ' . number_format($data->itrx_total_pembelian , '0',',','.'), 0, 1, 'L');
        }
        $fpdf->Cell(10, 10, '', 0, 1);


        $fpdf->SetFont('Courier', 'B', 10);
        $fpdf->Cell(35, 10, 'Nama Barang', 1, 0, 'C');
        $fpdf->Cell(52, 10, 'Harga', 1, 0, 'C');
        $fpdf->Cell(50, 10, 'Jumlah', 1, 0, 'C');
        $fpdf->Cell(50, 10, 'Sub Total', 1, 1, 'C');

        $fpdf->SetFont('Courier', '', 10);
        foreach ($detailTransaksi as $data){
            $fpdf->Cell(35, 10, $data->itrx_nama_barang, 1, 0, 'L');
            $fpdf->Cell(52, 10, 'Rp. '. number_format($data->itrx_harga_barang,'0',',','.'), 1, 0,'C');
            $fpdf->Cell(50, 10, $data->itrx_jumlah_barang, 1, 0,'C');
            $fpdf->Cell(50, 10, 'Rp. '. number_format($data->itrx_total_pembelian, '0',',','.'), 1, 1,'C');
        }
        $fpdf->Cell(10, 15, '', 0, 1);


        $fpdf->Output('', $name);
        exit;
    }


    public function cetakTransaksiPembelianPdf()
    {
        $dataTrxPembelian = DB::table('transaksi_pembelian')->select('nomor_transaksi', 'tanggal_pembelian', 'nama_karyawan')
            ->join('employees', 'employees.id_karyawan', '=', 'transaksi_pembelian.id_karyawan')->get();

        $fpdf = new FPDF('P', 'mm', 'A4');
        $tanggalDicetak = date('d M Y');
        $dicetakOleh = session('namaUserLogin');

        $fpdf->AddPage();

        $fpdf->SetFont('Courier', 'B', '20');
        $fpdf->Cell('0', '10', 'SITOKER', 0, 1, 'C');
        $fpdf->SetFont('Courier', 'B', '15');
        $fpdf->Cell('0', '10', 'Rekap Data Transaksi Pembelian', 0, 1, 'C');
        $fpdf->SetFont('Courier', 'B', '20');
        $fpdf->Cell(10, 5, '---------------------------------------------', 0, 1);

        $fpdf->Cell(10, 7, '', 0, 1);
        $fpdf->SetFont('Courier', '', 12);
        $fpdf->Cell(15, 10, "Dicetak Tanggal        : " . $tanggalDicetak, 0, 1);
        $fpdf->Cell(15, 10, "Dicetak Oleh           : " . $dicetakOleh);

        $fpdf->Cell(10, 15, '', 0, 1);

        $fpdf->SetFont('Courier', '', 10);
        $fpdf->Cell(10, 10, 'No', 1, 0, 'C');
        $fpdf->Cell(50, 10, 'Kode Transaksi', 1, 0, 'C');
        $fpdf->Cell(55, 10, 'Tanggal Pembelian', 1, 0, 'C');
        $fpdf->Cell(80, 10, 'Karyawan Yang Melayani Trx Pembelian', 1, 1, 'C');


        $no = 1;
        foreach ($dataTrxPembelian as $data) {
            $fpdf->Cell(10, 10, $no, 1, 0, 'C');
            $fpdf->Cell(50, 10, $data->nomor_transaksi, 1, 0, 'L');
            $fpdf->Cell(55, 10, $data->tanggal_pembelian . " WIB", 1, 0, 'L');
            $fpdf->Cell(80, 10, $data->nama_karyawan, 1, 1, 'L');
            $no++;
        }


        $fpdf->SetFont('Courier', '', '12');

        $fpdf->Output('D', 'Rekap Data Transaksi Pembelian.pdf');
        exit;

    }

    public function exportTransaksiPembelianExcel()
    {

        $dataTrxPembelian = DB::table('transaksi_pembelian')->select('nomor_transaksi', 'tanggal_pembelian', 'nama_karyawan')
            ->join('employees', 'employees.id_karyawan', '=', 'transaksi_pembelian.id_karyawan')->get();

        $tanggalDicetak = date('d M Y');
        $dicetakOleh = session('namaUserLogin');


        $filename = "Rekap Data Transaksi Pembelian.xls";

        $excel = new Spreadsheet();

        $sheet = $excel->getActiveSheet();
        $sheet->setCellValue('A1', 'SITOKER')->mergeCells('A1:E1');
        $sheet->setCellValue('A2', 'Rekap Data Transaksi Pembelian')->mergeCells('A2:E2');

        $sheet->setCellValue('A4', 'Dicetak Tanggal : ' . $tanggalDicetak)->mergeCells('A4:E4');
        $sheet->setCellValue('A5', 'Dicetak Oleh : ' . $dicetakOleh)->mergeCells('A5:E5');

        $sheet->setCellValue('A7', 'NO');
        $sheet->setCellValue('B7', 'Kode Transaksi');
        $sheet->setCellValue('C7', 'Tanggal Pembelian');
        $sheet->setCellValue('D7', 'Karyawan Yang Melayani Transaksi Pembelian');

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);


        $row = 8;
        $no = 1;
        foreach ($dataTrxPembelian as $data) {
            $sheet->setCellValue("A$row", $no++);
            $sheet->setCellValue("B$row", $data->nomor_transaksi);
            $sheet->setCellValue("C$row", $data->tanggal_pembelian);
            $sheet->setCellValue("D$row", $data->nama_karyawan);
            $row++;
        }

        $writer = new Xls($excel);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
    }
}
