<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\BaseController;
use Illuminate\Support\Facades\DB;

class AjaxController extends BaseController
{
    public function getBarangByKode()
    {
        $kodeBarang = request('idbarang');
        $query = DB::table('products')
            ->where('id_barang', '=', $kodeBarang)
            ->first();
        if ($query === null) {
            return response()->json([], 400);
        }
        return response()->json($query);
    }

    public function getKodeMember()
    {
        $kodeMember = request('kodeMember');
        $query = DB::table('members')
            ->where('id_member', '=', $kodeMember)
            ->first();
        if ($query === null) {
            return response()->json([], 400);
        }
        return response()->json($query);
    }
}
