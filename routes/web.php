<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/login', function () {
//    return view('layout/login');
//});

//login

Route::get('', 'App\Http\Controllers\Auth\LoginController@index')->name('login.index');
Route::post('/login/verify', 'App\Http\Controllers\Auth\LoginController@verifyLogin')->name('login.verify');


Route::group(['prefix' => 'sitoker', 'middleware' => ['web', 'tokoretail.middleware']], static function () {
    Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->name('dashboard.index');


    //Barang
    Route::get('/products', 'App\Http\Controllers\ProductController@index')->name('product.index');
    Route::post('/products/create', 'App\Http\Controllers\ProductController@addProduct')->name('product.create');
    Route::get('/products/update/{idBarang}', 'App\Http\Controllers\ProductController@updateProduct')->name('product.update');
    Route::get('/products/cetakPdf', 'App\Http\Controllers\ProductController@cetakProductsPdf')->name('products.cetakpdf');
    Route::get('/products/cetakExcel', 'App\Http\Controllers\ProductController@exportProductExcel')->name('products.cetakExcel');

    //route member
    Route::get('/member', 'App\Http\Controllers\MemberController@index')->name('member.index');
    Route::post('/members', 'App\Http\Controllers\MemberController@createMember')->name('createmember.index');
    Route::get('/members/update/{idMember}', 'App\Http\Controllers\MemberController@editMember')->name('member.update');
    Route::get('/members/update_status/{idMember}', 'App\Http\Controllers\MemberController@statusMember')->name('member.status_update');
    Route::get('/members/cetakPdf', 'App\Http\Controllers\MemberController@cetakMembersPdf')->name('member.cetakpdf');
    Route::get('/members/cetakKartuMember/{id_member}', 'App\Http\Controllers\MemberController@cetakKartuMember')->name('member.cetakKartuMember');
    Route::get('/members/cetakExcel', 'App\Http\Controllers\MemberController@exportMembersExcel')->name('member.cetakExcel');

    //route user
    Route::get('/user', 'App\Http\Controllers\UserController@index')->name('user.index');
    Route::post('/users', 'App\Http\Controllers\UserController@proses_createUser')->name('createuser.index');
    Route::get('/users/update/{id}', 'App\Http\Controllers\UserController@editDataUser')->name('user.update');
    Route::get('/users/cetakPdf', 'App\Http\Controllers\UserController@cetakUserPdf')->name('user.cetakpdf');
    Route::get('/users/ubahPassword', 'App\Http\Controllers\UserController@ubahPassword')->name('user.ubahpassword');
    Route::get('/users/cetakExcel', 'App\Http\Controllers\UserController@exportUserExcel')->name('user.cetakexcel');

//    Route::post('/users/{id}','App\Http\Controllers\UserController@proses_createUser')->name('updateuser.index');

    //Supplier
    Route::get('/suppliers', 'App\Http\Controllers\SupplierController@index')->name('supplier.index');
    Route::post('/suppliers/add', 'App\Http\Controllers\SupplierController@addSupplier')->name('supplier.add');
    Route::get('/suppliers/update/{idSupplier}', 'App\Http\Controllers\SupplierController@addSupplier')->name('supplier.update');
    Route::get('/suppliers/cetakPdf', 'App\Http\Controllers\SupplierController@cetakSupplierPdf')->name('supplier.cetakpdf');
    Route::get('/suppliers/cetakExcel', 'App\Http\Controllers\SupplierController@exportSupplierExcel')->name('supplier.Excel');


    //route karyawan
    Route::get('/employees', 'App\Http\Controllers\KaryawanController@index')->name('karyawan.index');
    Route::post('/employees', 'App\Http\Controllers\KaryawanController@createKaryawan')->name('createkaryawan.index');
    Route::post('/employees/update/{idKaryawan}', 'App\Http\Controllers\KaryawanController@editKaryawan')->name('karyawan.update');
    Route::get('/employees/update/{idKaryawan}', 'App\Http\Controllers\KaryawanController@statusKaryawan')->name('karyawan.status_update');
    Route::get('/employees/cetakPdf', 'App\Http\Controllers\KaryawanController@cetakEmployeesPdf')->name('karyawan.cetakpdf');
    Route::get('/employees/cetakExcel', 'App\Http\Controllers\KaryawanController@exportKaryawanExcel')->name('karyawan.cetakExcel');


    //route app_trans_jual
    Route::get('/app_trans_jual', 'App\Http\Controllers\Transaksi\TransaksiPenjualanController@index')->name('trans_jual.index');
    Route::get('/app_trans_jual/trans', 'App\Http\Controllers\Transaksi\TransaksiPenjualanController@formTransaksiJual')->name('app.trans');
    Route::post('/app_trans_jual/prosesTrans', 'App\Http\Controllers\Transaksi\TransaksiPenjualanController@prosesTransaksi')->name('app.proses.trans');
    Route::get('/app_trans_jual/cetakPdf', 'App\Http\Controllers\Transaksi\TransaksiPenjualanController@cetakTransaksiPenjualanPdf')->name('trxPenjualan.cetakpdf');
    Route::get('/app_trans_jual/cetakExcel', 'App\Http\Controllers\Transaksi\TransaksiPenjualanController@exportTransaksiPenjualanExcel')->name('trxPenjualan.cetakExcel');
    Route::get('/app_trans_jual/trans/detail/{id_transaksi_penjualan}', 'App\Http\Controllers\Transaksi\TransaksiPenjualanController@detailTransaksi')->name('detail.trans.penjualan');
    Route::get('/app_trans/cetakInvoiceTransaksi/{id_transaksi_penjualan}', 'App\Http\Controllers\Transaksi\TransaksiPenjualanController@cetakInvoiceTransaksi')->name('trans.cetakinvoice');

    //Transaksi Pembelian
    Route::get('/transaksi-pembelian', 'App\Http\Controllers\Transaksi\TransaksiPembelianController@index')->name('transaksi.pembelian.index');
    Route::get('/transaksi-pembelian/addTrx', 'App\Http\Controllers\Transaksi\TransaksiPembelianController@addTransaksi')->name('transaksi.pembelian.add');
    Route::post('transaksi-pembelian/prosesTransaksi', 'App\Http\Controllers\Transaksi\TransaksiPembelianController@prosesTransaksiPembelian')->name('transaksi.pembelian.proses');
    Route::get('/transaksi-pembelian/cetakPdf', 'App\Http\Controllers\Transaksi\TransaksiPembelianController@cetakTransaksiPembelianPdf')->name('trxPembelian.cetakpdf');
    Route::get('/transaksi-pembelian /cetakExcel', 'App\Http\Controllers\Transaksi\TransaksiPembelianController@exportTransaksiPembelianExcel')->name('trxPembelian.cetakExcel');
    Route::get('/transaksi_pembelian/detail/{id_transaksi_pembelian}', 'App\Http\Controllers\Transaksi\TransaksiPembelianController@detailTransaksi')->name('detail.trans.pembelian');
    Route::get('/transaksi_pembelian/detail/cetakInvoice/{id_transaksi_pembelian}', 'App\Http\Controllers\Transaksi\TransaksiPembelianController@cetakInvoiceTransaksi')->name('trans.cetakinvoice');

    //ajax get barang by code
    Route::post('/ajax/getBarangByKode','App\Http\Controllers\AjaxController@getBarangByKode')->name('ajax.getBarangByKode');

    //Get Kode member ajax
    Route::post('/ajax/getKodeMember','App\Http\Controllers\AjaxController@getKodeMember')->name('ajax.getKodeMember');
});

Route::get('/logout', 'App\Http\Controllers\Auth\LoginController@logout')->name('logout.index');

    //ajax Controller
    Route::post('/ajax/getBarangByKode','App\Http\Controllers\AjaxController@getBarangByKode')->name('ajax.getBarangByKode');
//
//#Login
//Route::get('app/login', [LoginController::class, 'login']);
//#Dashboard
//Route::group(['prefix' => 'app'], function (){
//   Route::get('/dashboard', [DashboardController::class, 'index']);
//});
